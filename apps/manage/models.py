# -*- coding: utf-8 -*-
from django.db import models
from django.db.models import Q
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import User
from content import *

class PhanQuyen(models.Model):
    tenquyen = models.CharField(max_length=50, verbose_name="Tên quyền")
    truongchinhsua = models.CharField(max_length=255, verbose_name="Trường chỉnh sửa", null=True, blank=True)

    def __unicode__(self):
        return self.tenquyen

class NguoiDung(models.Model):
    user = models.ForeignKey(User)
    tennguoidung = models.CharField(max_length=255, verbose_name="Tên người dùng")
    phanquyen = models.ForeignKey(PhanQuyen)

    def __unicode__(self):
        return self.tennguoidung

class HoGiaDinh(models.Model):
    masoho = models.CharField(max_length=50, verbose_name="Mã số hộ (*)", unique=True, help_text="Làm ơn sử dụng định dạng sau: Txxx-xx")
    sonha = models.CharField(max_length=255, verbose_name="Địa chỉ nhà (*)")
    todanpho = models.CharField(max_length=255,  verbose_name="Tổ dân phố (*)")
    tinhtrangcutru = models.CharField(choices=CUTRU_CHOICES, max_length=255,  verbose_name="Tình trạng cư trú (*)")
    thuochongheo = models.CharField(choices=HONGHEO_CHOICES, max_length=255, verbose_name="Thuộc hộ nghèo")
    thuochochinhsach = models.BooleanField(verbose_name="Hộ có đối tượng chính sách")
    thuochocodoituongbaotro = models.BooleanField(verbose_name="Hộ có đội tượng bảo trợ xã hội")
    cogiaychungnhanquyensdd = models.CharField(max_length=255, verbose_name="Giấy chứng nhận quyền sử dụng đất (*)")
    sodienthoai = models.CharField(max_length=255, verbose_name="Số điện thoại (*)")
    tienthue = models.CharField(max_length=255, verbose_name="Tiền thuế (*)")
    chuho = models.IntegerField(null=True, verbose_name="Chủ hộ", blank=True, default=1)

    def __unicode__(self):
        return self.masoho

class ThanhVien(models.Model):
    hogiadinh = models.ForeignKey(HoGiaDinh, verbose_name="Hộ gia đình (*)")
    hotenthanhvien = models.CharField(max_length=255, verbose_name="Họ tên thành viên (*)")
    ngaysinh = models.DateField(verbose_name="Ngày sinh (*)")
    gioitinh = models.CharField(default="Nam", max_length=10, verbose_name="Giới tính (*)")
    quequan = models.CharField(max_length=255, verbose_name="Quê quán (*)")
    quanhevoichuho = models.CharField(max_length=255, verbose_name="Quan hệ với chủ hộ (*)")
    thuocdoituongbodoixuatngu = models.BooleanField(blank=True, default=False, verbose_name="Thuộc đối tượng bộ đội xuất ngũ")
    thuocdoituongbaotro = models.CharField(blank=True, choices=BTXH_CHOICES, max_length=255, verbose_name="Thuộc đối tượng bảo trợ")
    thuocdoituongxichlo = models.BooleanField(blank=True, default=False, verbose_name="Thuộc đối tượng xích lô")
    cmnd = models.CharField(blank=True, max_length=255, verbose_name="Số chứng minh nhân dân")
    thuocdoituongkhuyetat = models.CharField(blank=True, choices=KHUYETTAT_CHOICES, max_length=255, verbose_name="Thuộc đối tượng khuyết tật")
    thuocdoituongchinhsach = models.CharField(blank=True, max_length=255, choices=CS_CHOICES, verbose_name="Thuộc đối tượng chính sách", )
    cobaohiem = models.BooleanField(blank=True, default=False, verbose_name="Có bảo hiểm")
    tinhtranghonnhan = models.CharField(blank=True, choices=HONNHAN_CHOICES, max_length=50, verbose_name="Tình trạng hôn nhân")
    trinhdo = models.CharField(blank=True, max_length=255, verbose_name="Trình độ")
    chuyenmon = models.CharField(blank=True, max_length=255, verbose_name="Chuyên môn")
    tongiao = models.CharField(blank=True, max_length=255,  verbose_name="Tôn giáo")
    dantoc = models.CharField(blank=True, max_length=255, verbose_name="Dân tộc")

    def __unicode__(self):
        return self.hotenthanhvien

    def thuocdoituongchinhsach_as_list(self):
        return self.thuocdoituongchinhsach.split(';')

    def thuocdoituongbaotro_as_list(self):
        return self.thuocdoituongbaotro.split(';')