from django.conf.urls.defaults import patterns, include, url
from django.contrib.auth.views import password_reset
# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'manage.views.index', name='home'),
    url(r'^dangnhap/$', 'manage.views.login', name='login'),
    url(r'^dangxuat/$', 'manage.views.logout', {'next_page': '/dangnhap'}),
    url(r'^laylaimatkhau/$', password_reset, {'post_reset_redirect': '/dangnhap/', 'template_name': 'laylaimatkhau.html'}),
    #url(r'^dangnhap/$', 'django.contrib.auth.views.login', {'template_name': 'login.html'}),
    url(r'^hogiadinh/(?P<masoho>.*)$', 'manage.views.hogiadinh', name='hogiadinh'),
    url(r'^themhogiadinh/$', 'manage.views.themhogiadinh', name='themhogiadinh'),
    url(r'^suahogiadinh/(?P<masoho>.*)$', 'manage.views.suahogiadinh', name='suahogiadinh'),
    url(r'^themthanhvien/(?P<masoho>.*)$', 'manage.views.themthanhvien', name='themthanhvien'),
    url(r'^suathanhvien/(?P<mathanhvien>.*)$', 'manage.views.suathanhvien', name='suathanhvien'),
    url(r'^nhapxls/(?P<file_type>.*)$', 'manage.views.nhapxls', name='nhapxls'),
    url(r'^nhapxls/$', 'manage.views.nhapxls', name='nhapxls'),
    url(r'^nguoidung/$', 'manage.views.nguoidung', name='nguoidung'),
    url(r'^nguoidung/(?P<manguoidung>\d+)$', 'manage.views.nguoidung', name='nguoidung'),
    url(r'^phanquyen/(?P<maquyen>\d+)$', 'manage.views.phanquyen', name='phanquyen'),
    url(r'^phanquyen/$', 'manage.views.phanquyen', name='phanquyen'),
    url(r'^taikhoan/$', 'manage.views.taikhoan', name='taikhoan'),
    
    ################### TIM KIEM ########################################
    url(r'^timkiemhogiadinh/$', 'manage.views.timkiemhogiadinh', name='timkiemhogiadinh'),
    url(r'^xuattimkiemhogiadinh/$', 'manage.views.xuattimkiemhogiadinh', name='xuattimkiemhogiadinh'),
    url(r'^timkiemthanhvien/$', 'manage.views.timkiemthanhvien', name='timkiemthanhvien'),
    url(r'^xuattimkiemthanhvien/$', 'manage.views.xuattimkiemthanhvien', name='xuattimkiemthanhvien'),
    
    ################### BAO CAO - THONG KE ########################################
    url(r'^danhsachchuho/$', 'manage.views.danhsachchuho', name='danhsachchuho'),
    url(r'^xuatdanhsachchuho/$', 'manage.views.xuatdanhsachchuho', name='xuatdanhsachchuho'),
    url(r'^danhsachhogiadinh/$', 'manage.views.danhsachhogiadinh', name='danhsachhogiadinh'),
    url(r'^xuatdanhsachhogiadinh/$', 'manage.views.xuatdanhsachhogiadinh', name='xuatdanhsachhogiadinh'),
    url(r'^danhsachthuongbenhbinh/$', 'manage.views.danhsachthuongbenhbinh', name='danhsachthuongbenhbinh'),
    url(r'^xuatdanhsachthuongbenhbinh/$', 'manage.views.xuatdanhsachthuongbenhbinh', name='xuatdanhsachthuongbenhbinh'),
    url(r'^danhsachhongheo/$', 'manage.views.danhsachhongheo', name='danhsachhongheo'),
    url(r'^xuatdanhsachhongheo/$', 'manage.views.xuatdanhsachhongheo', name='xuatdanhsachhongheo'),
    url(r'^danhsachtreemduoi6/$', 'manage.views.danhsachtreemduoi6', name='danhsachtreemduoi6'),
    url(r'^xuatdanhsachtreemduoi6/$', 'manage.views.xuatdanhsachtreemduoi6', name='xuatdanhsachtreemduoi6'),
    url(r'^danhsachdoituongchinhsach/$', 'manage.views.danhsachdoituongchinhsach', name='danhsachdoituongchinhsach'),
    url(r'^xuatdanhsachdoituongchinhsach/$', 'manage.views.xuatdanhsachdoituongchinhsach', name='xuatdanhsachdoituongchinhsach'),
    url(r'^danhsachdoituongbaotro/$', 'manage.views.danhsachdoituongbaotro', name='danhsachdoituongbaotro'),
    url(r'^xuatdanhsachdoituongbaotro/$', 'manage.views.xuatdanhsachdoituongbaotro', name='xuatdanhsachdoituongbaotro'),
    url(r'^danhsachdoituongbodoixuatngu/$', 'manage.views.danhsachdoituongbodoixuatngu', name='danhsachdoituongbodoixuatngu'),
    url(r'^xuatdanhsachdoituongbodoixuatngu/$', 'manage.views.xuatdanhsachdoituongbodoixuatngu', name='xuatdanhsachdoituongbodoixuatngu'),
)
