# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'PhanQuyen'
        db.create_table('manage_phanquyen', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('tenquyen', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('truongchinhsua', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('manage', ['PhanQuyen'])

        # Adding model 'NguoiDung'
        db.create_table('manage_nguoidung', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('tennguoidung', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('phanquyen', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['manage.PhanQuyen'])),
        ))
        db.send_create_signal('manage', ['NguoiDung'])

        # Adding model 'HoGiaDinh'
        db.create_table('manage_hogiadinh', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('masoho', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('sonha', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('todanpho', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('tinhtrangcutru', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('thuochongheo', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('thuochochinhsach', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('cogiaychungnhanquyensdd', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('sodienthoai', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('tienthue', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('chuho', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('manage', ['HoGiaDinh'])

        # Adding model 'ThanhVien'
        db.create_table('manage_thanhvien', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('hogiadinh', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['manage.HoGiaDinh'])),
            ('hotenthanhvien', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('ngaysinh', self.gf('django.db.models.fields.DateField')()),
            ('gioitinh', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('quequan', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('quanhevoichuho', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('thuocdoituongbodoixuatngu', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('thuocdoituongbaotro', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('thuocdoituongxichlo', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('cmnd', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('thuocdoituongkhuyetat', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('thuocdoituongchinhsach', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('cobaohiem', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('tinhtranghonnhan', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('trinhdo', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('chuyenmon', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('tongiao', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('dantoc', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
        ))
        db.send_create_signal('manage', ['ThanhVien'])


    def backwards(self, orm):
        # Deleting model 'PhanQuyen'
        db.delete_table('manage_phanquyen')

        # Deleting model 'NguoiDung'
        db.delete_table('manage_nguoidung')

        # Deleting model 'HoGiaDinh'
        db.delete_table('manage_hogiadinh')

        # Deleting model 'ThanhVien'
        db.delete_table('manage_thanhvien')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'manage.hogiadinh': {
            'Meta': {'object_name': 'HoGiaDinh'},
            'chuho': ('django.db.models.fields.IntegerField', [], {}),
            'cogiaychungnhanquyensdd': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'masoho': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'sodienthoai': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'sonha': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'thuochochinhsach': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'thuochongheo': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'tienthue': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'tinhtrangcutru': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'todanpho': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'manage.nguoidung': {
            'Meta': {'object_name': 'NguoiDung'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'phanquyen': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['manage.PhanQuyen']"}),
            'tennguoidung': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'manage.phanquyen': {
            'Meta': {'object_name': 'PhanQuyen'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tenquyen': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'truongchinhsua': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'manage.thanhvien': {
            'Meta': {'object_name': 'ThanhVien'},
            'chuyenmon': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'cmnd': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'cobaohiem': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'dantoc': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'gioitinh': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'hogiadinh': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['manage.HoGiaDinh']"}),
            'hotenthanhvien': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ngaysinh': ('django.db.models.fields.DateField', [], {}),
            'quanhevoichuho': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'quequan': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'thuocdoituongbaotro': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'thuocdoituongbodoixuatngu': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'thuocdoituongchinhsach': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'thuocdoituongkhuyetat': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'thuocdoituongxichlo': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'tinhtranghonnhan': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'tongiao': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'trinhdo': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'})
        }
    }

    complete_apps = ['manage']