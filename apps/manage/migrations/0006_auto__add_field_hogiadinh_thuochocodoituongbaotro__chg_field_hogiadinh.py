# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'HoGiaDinh.thuochocodoituongbaotro'
        db.add_column('manage_hogiadinh', 'thuochocodoituongbaotro',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)


        # Changing field 'HoGiaDinh.thuochongheo'
        db.alter_column('manage_hogiadinh', 'thuochongheo', self.gf('django.db.models.fields.CharField')(max_length=255))

        # Changing field 'HoGiaDinh.thuochochinhsach'
        db.alter_column('manage_hogiadinh', 'thuochochinhsach', self.gf('django.db.models.fields.BooleanField')())

    def backwards(self, orm):
        # Deleting field 'HoGiaDinh.thuochocodoituongbaotro'
        db.delete_column('manage_hogiadinh', 'thuochocodoituongbaotro')


        # Changing field 'HoGiaDinh.thuochongheo'
        db.alter_column('manage_hogiadinh', 'thuochongheo', self.gf('django.db.models.fields.BooleanField')())

        # Changing field 'HoGiaDinh.thuochochinhsach'
        db.alter_column('manage_hogiadinh', 'thuochochinhsach', self.gf('django.db.models.fields.CharField')(max_length=255))

    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'manage.hogiadinh': {
            'Meta': {'object_name': 'HoGiaDinh'},
            'chuho': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'cogiaychungnhanquyensdd': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'masoho': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'sodienthoai': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'sonha': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'thuochochinhsach': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'thuochocodoituongbaotro': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'thuochongheo': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'tienthue': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'tinhtrangcutru': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'todanpho': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'manage.nguoidung': {
            'Meta': {'object_name': 'NguoiDung'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'phanquyen': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['manage.PhanQuyen']"}),
            'tennguoidung': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'manage.phanquyen': {
            'Meta': {'object_name': 'PhanQuyen'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tenquyen': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'truongchinhsua': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        'manage.thanhvien': {
            'Meta': {'object_name': 'ThanhVien'},
            'chuyenmon': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'cmnd': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'cobaohiem': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'dantoc': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'gioitinh': ('django.db.models.fields.CharField', [], {'default': "'Nam'", 'max_length': '10'}),
            'hogiadinh': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['manage.HoGiaDinh']"}),
            'hotenthanhvien': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ngaysinh': ('django.db.models.fields.DateField', [], {}),
            'quanhevoichuho': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'quequan': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'thuocdoituongbaotro': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'thuocdoituongbodoixuatngu': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'thuocdoituongchinhsach': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'thuocdoituongkhuyetat': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'thuocdoituongxichlo': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'tinhtranghonnhan': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'tongiao': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'trinhdo': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['manage']