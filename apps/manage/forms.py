# -*- coding: utf-8 -*-
from django import forms
from django.contrib.auth.models import User, Group
from django.forms import ModelForm, Form
from models import *
import re
from content import *

class HoGiaDinhAddForm(ModelForm):
    masoho = forms.CharField(label='Mã số hộ (*)', help_text="(Hãy sử dụng định dạng sau: Txxx-xx)")

    class Meta:
        model = HoGiaDinh
        exclude = ('chuho')

    def clean_masoho(self):
        masoho = self.cleaned_data['masoho']
        if self.cleaned_data is None:
            raise forms.ValidationError('Mã số hộ được yêu cầu.')
        else:
            hogiadinh = HoGiaDinh.objects.filter(masoho=masoho)
            if hogiadinh.count()>0:
                raise forms.ValidationError('Mã số hộ này đã tồn tại. Yêu cầu nhập một mã số hộ khác.')
            if re.match('^T\d{3}-\d{2}$', masoho) is None:
                raise forms.ValidationError('Mã số hộ này không đúng định dạng. Hãy nhập theo đúng định dạng Txxx-xx')
        return masoho

    def __init__(self, *args, **kwargs):
        super(HoGiaDinhAddForm, self).__init__(*args, **kwargs)
        messages = {'masoho': 'Mã số hộ',
                    'sonha': 'Số nhà',
                    'todanpho': 'Tổ dân phố',
                    'tinhtrangcutru': 'Tình trạng cư trú',
                    'thuochongheo': 'Thuộc hộ nghèo',
                    'thuochochinhsach': 'Thuộc hộ chính sách',
                    'sodienthoai': 'Số điện thoại',
                    'tienthue': 'Tiền thuế'
                    }

        for field in self.fields:
            if field in messages:
                self.fields[field].error_messages = {'required': messages[field] + ' được yêu cầu.'}

class HoGiaDinhEditForm(ModelForm):
    masoho = forms.CharField(label='Mã số hộ (*)', help_text="(Hãy ơn sử dụng định dạng sau: Txxx-xx)")

    class Meta:
        model = HoGiaDinh
        exclude = ('chuho')

    def clean_masoho(self):
        masoho = self.cleaned_data['masoho']
        if self.cleaned_data is None:
            raise forms.ValidationError('Mã số hộ được yêu cầu.')
        else:
            if re.match('^T\d{3}-\d{2}$', masoho) is None:
                raise forms.ValidationError('Mã số hộ này không đúng định dạng. Hãy nhập theo đúng định dạng Txxx-xx')
        return masoho

    def __init__(self, *args, **kwargs):
        super(HoGiaDinhEditForm, self).__init__(*args, **kwargs)
        messages = {'masoho': 'Mã số hộ',
                    'sonha': 'Số nhà',
                    'todanpho': 'Tổ dân phố',
                    'tinhtrangcutru': 'Tình trạng cư trú',
                    'thuochongheo': 'Thuộc hộ nghèo',
                    'thuochochinhsach': 'Thuộc hộ chính sách',
                    'sodienthoai': 'Số điện thoại',
                    'tienthue': 'Tiền thuế'
                    }

        for field in self.fields:
            if field in messages:
                self.fields[field].error_messages = {'required': messages[field] + ' được yêu cầu.'}

class ThanhVienAddForm(ModelForm):
    gioitinh = forms.ChoiceField(label='Giới tính (*)', choices=GENDER_CHOICES, widget=forms.RadioSelect(attrs={'class':'special'}))
    thuocdoituongchinhsach = forms.MultipleChoiceField(widget=forms.SelectMultiple, choices=CS_CHOICES, required=False, label="Thuộc đối tượng chính sách")
    thuocdoituongbaotro = forms.MultipleChoiceField(widget=forms.SelectMultiple, choices=BTXH_CHOICES, required=False, label="Thuộc đối tượng bảo trợ")

    def clean_thuocdoituongchinhsach(self):
        ds = ""
        for d in self.cleaned_data['thuocdoituongchinhsach']:
            ds = ds + d + ";"
        return ds
    
    def clean_thuocdoituongbaotro(self):
        ds = ""
        for d in self.cleaned_data['thuocdoituongbaotro']:
            ds = ds + d + ";"
        return ds

    class Meta:
        model = ThanhVien
        exclude = ('hogiadinh', 'thuocdoituongchinhsach', 'thuocdoituongbaotro')

    def __init__(self, *args, **kwargs):
        super(ThanhVienAddForm, self).__init__(*args, **kwargs)
        messages = {'hotenthanhvien': 'Họ tên thành viên',
                    'ngaysinh': 'Ngày sinh',
                    'gioitinh': 'Giới tính',
                    'quequan' : 'Quê quán',
                    'quanhevoichuho': 'Quan hệ với chủ hộ',
                    'thuocdoituongbodoixuatngu': 'Thuộc đối tượng bộ dội xuất ngũ',
                    'thuocdoituongbaotro': 'Thuộc đối tượng bảo trợ',
                    'thuocdoituongxichlo': 'Thuộc đối tượng xích lô',
                    'cmnd': 'Số chứng minh nhân dân',
                    'thuocdoituongkhuyetat': 'Thuộc đối tượng khuyết tật',
                    'thuocdoituongchinhsach': 'Thuộc đối tượng chính sách',
                    'cobaohiem': 'Có bảo hiểm',
                    'tinhtranghonnhan': 'Tình trạng hôn nhân',
                    'trinhdo': 'Trình độ',
                    'chuyenmon': 'Chuyên môn',
                    'tongiao': 'Tôn giáo',
                    'dantoc': 'Dân tộc'
                    }
        for field in self.fields:
            if field in messages:
                self.fields[field].error_messages = {'required': messages[field] + ' được yêu cầu.'}

        #del self.fields['ngaysinh']

class ThanhVienEditForm(ModelForm):
    gioitinh = forms.ChoiceField(label='Giới tính (*)', choices=GENDER_CHOICES, widget=forms.RadioSelect(attrs={'class':'special'}))
    thuocdoituongchinhsach = forms.MultipleChoiceField(widget=forms.SelectMultiple, choices=CS_CHOICES, required=False, label="Thuộc đối tượng chính sách")
    thuocdoituongbaotro = forms.MultipleChoiceField(widget=forms.SelectMultiple, choices=BTXH_CHOICES, required=False, label="Thuộc đối tượng bảo trợ")

    def clean_thuocdoituongchinhsach(self):
        ds = ""
        for d in self.cleaned_data['thuocdoituongchinhsach']:
            ds = ds + d + ";"
        return ds
    
    def clean_thuocdoituongbaotro(self):
        ds = ""
        for d in self.cleaned_data['thuocdoituongbaotro']:
            ds = ds + d + ";"
        return ds

    class Meta:
        model = ThanhVien
        exclude = ('hogiadinh', 'thuocdoituongchinhsach', 'thuocdoituongbaotro')

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super(ThanhVienEditForm, self).__init__(*args, **kwargs)
        messages = {'hotenthanhvien': 'Họ tên thành viên',
                    'ngaysinh': 'Ngày sinh',
                    'gioitinh': 'Giới tính',
                    'quequan' : 'Quê quán',
                    'quanhevoichuho': 'Quan hệ với chủ hộ',
                    'thuocdoituongbodoixuatngu': 'Thuộc đối tượng bộ dội xuất ngũ',
                    'thuocdoituongbaotro': 'Thuộc đối tượng bảo trợ',
                    'thuocdoituongxichlo': 'Thuộc đối tượng xích lô',
                    'cmnd': 'Số chứng minh nhân dân',
                    'thuocdoituongkhuyetat': 'Thuộc đối tượng khuyết tật',
                    'thuocdoituongchinhsach': 'Thuộc đối tượng chính sách',
                    'cobaohiem': 'Có bảo hiểm',
                    'tinhtranghonnhan': 'Tình trạng hôn nhân',
                    'trinhdo': 'Trình độ',
                    'chuyenmon': 'Chuyên môn',
                    'tongiao': 'Tôn giáo',
                    'dantoc': 'Dân tộc'
                    }
        list = []
        for field in self.fields:
            if field in messages:
                self.fields[field].error_messages = {'required': messages[field] + ' được yêu cầu.'}
            if field not in self.user.get_profile().phanquyen.truongchinhsua.split(', '):
                list.append(field)

        for l in list:
            del self.fields[l]
        
        #self.fields['thuocdoituongchinhsach'].value = ['Bà mẹ VNAH']
        
class LoginForm(Form):
    username = forms.CharField(required=True)
    password = forms.CharField(required=True)

class UploadFileForm(Form):
    hogiadinh_file = forms.FileField(required=False)
    thanhvien_file = forms.FileField(required=False)

class NguoidungForm(Form):
    fullname = forms.CharField(required=True, label="Tên người dùng")
    username = forms.CharField(required=True, label="Tên đăng nhập")
    password = forms.CharField(required=True, label="Mật khẩu")
    re_password = forms.CharField(required=True, label="Nhập lại mật khẩu")
    permission = forms.ModelChoiceField(queryset = PhanQuyen.objects.all(), label="Phân quyền", required=True)

    def __init__(self, *args, **kwargs):
        super(NguoidungForm, self).__init__(*args, **kwargs)
        messages = {'fullname': 'Họ tên thành viên',
                    'username': 'Tên đăng nhập',
                    'password': 'Mật khẩu',
                    're_password': 'Lặp lại mật khẩu',
                    'permission': 'Phân quyền'
                    }
        for field in self.fields:
            if field in messages:
                self.fields[field].error_messages = {'required': messages[field] + ' được yêu cầu.'}
        
    def clean_re_password(self):
        if self.cleaned_data['password'] != self.cleaned_data['re_password']:
            raise forms.ValidationError('Mật khẩu nhập lại không đúng')
        else:
            return self.cleaned_data['re_password']

    def save(self, nguoidung):
        username = self.cleaned_data['username']
        fullname = self.cleaned_data['fullname']
        password = self.cleaned_data['password']
        re_password = self.cleaned_data['re_password']
        permission = self.cleaned_data['permission']

        if nguoidung is None:
            user = User()
            user.username = username
            user.email = username
            user.set_password(password)
            user.save()
            
            nguoidung = NguoiDung()
            nguoidung.user = user
            nguoidung.tennguoidung = fullname
            nguoidung.phanquyen = permission
            nguoidung.save()
        else:
            user = nguoidung.user
            user.username = username
            nguoidung.tennguoidung = fullname
            nguoidung.phanquyen = permission
            if len(password) < 50:
                user.set_password(password)
            user.save()
            nguoidung.save()

class PhanQuyenForm(Form):
    tenquyen = forms.CharField(required=True, label="Tên quyền", error_messages = {'required':'Tên quyền được yêu cầu.'})
    truongchinhsua = forms.MultipleChoiceField(label="Trường chỉnh sửa",
        widget=forms.CheckboxSelectMultiple, choices=PERMISSION_CHOICES)

    def __init__(self, *args, **kwargs):
        super(PhanQuyenForm, self).__init__(*args, **kwargs)
        messages = {'tenquyen': 'Tên phân quyền'}
        messages = {'truongchinhsua': 'Trường chỉnh sửa'}
        for field in self.fields:
            if field in messages:
                self.fields[field].error_messages = {'required': messages[field] + ' được yêu cầu.'}

    def save(self, quyen):
        tenquyen = self.cleaned_data['tenquyen']
        truongchinhsua = self.cleaned_data['truongchinhsua']
        str = ""
        for field in truongchinhsua:
            str = str + field + ", "
        if quyen is None:
            quyen = PhanQuyen()
            quyen.tenquyen = tenquyen
            quyen.truongchinhsua = str
        else:
            quyen.tenquyen = tenquyen
            quyen.truongchinhsua = str
        quyen.save()

class TimKiemHoForm(Form):
    masoho = forms.CharField(label="Mã số hộ")
    todanpho = forms.CharField(label="Tổ dân phố")
    thuochochinhsach = forms.ChoiceField(label="Hộ có đối tượng CS", choices=CHECKBOX_CHOICES, widget=forms.RadioSelect(attrs={'class':'special'}))
    thuochobaotro = forms.ChoiceField(label="Hộ có đối tượng BTXH", choices=CHECKBOX_CHOICES, widget=forms.RadioSelect(attrs={'class':'special'}))
    tinhtrangcutru = forms.ChoiceField(label="Tình trạng cư trú", choices=(('', '----------'),)+CUTRU_CHOICES)
    thuochongheo = forms.ChoiceField(label="Thuộc hộ nghèo", choices=(('', '----------'),)+HONGHEO_CHOICES)
    
class TimKiemThanhVienForm(Form):
    hotenthanhvien = forms.CharField(label="Tên", required=False)
    masoho = forms.CharField(label="Mã số hộ", required=False)
    todanpho = forms.CharField(label="Tổ dân phố", required=False)
    thuocdoituongchinhsach = forms.MultipleChoiceField(label="Đối tượng chính sách", required=False, choices=(('', '----------------------'),)+CS_CHOICES)
    thuocdoituongbaotro = forms.MultipleChoiceField(label="Đối tượng bảo trợ", required=False, choices=(('', '----------------------'),)+BTXH_CHOICES)
    tuoi = forms.ChoiceField(choices=AGE_CHOICES, required=False, widget=forms.RadioSelect(attrs={'class':'special'}))
    baohiem = forms.ChoiceField(label="Bảo hiểm", required=False, choices=YESNO_CHOICES, widget=forms.RadioSelect(attrs={'class':'special'}))
    tinhtrangcutru = forms.ChoiceField(label="Tình trạng cư trú", required=False, choices=(('', '---------------------'),)+CUTRU_CHOICES)
    thuocdoituongxichlo = forms.ChoiceField(label="Đối tượng xích lô", required=False, choices=YESNO_CHOICES, widget=forms.RadioSelect(attrs={'class':'special'}))
    thuocdoituongkhuyetat = forms.ChoiceField(label="Đối tượng khuyết tật", required=False, choices=(('', '---------------------'),)+KHUYETTAT_CHOICES)
    start_age = forms.CharField(label="Theo độ tuổi", required=False)
    end_age = forms.CharField(required=False)