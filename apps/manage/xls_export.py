#!/usr/bin/env python
# -*- coding: utf-8 -*- 
from xlwt import *
from django.db.models import Q
from models import PhanQuyen, NguoiDung, HoGiaDinh, ThanhVien
from utils import *

# Bold font for headers
bold_font = Font()
bold_font.bold = True
bold_style = XFStyle()
bold_style.font = bold_font

# Vertical align for cells
top_align_style = XFStyle()
alignment = Alignment()
alignment.vert = Alignment.VERT_TOP
top_align_style.alignment = alignment


def export_chuho():
    thanhvien = ThanhVien.objects.all()
    chuho = []
    for mem in thanhvien:
        if mem.id == mem.hogiadinh.chuho:
            chuho.append(mem)

    w = Workbook()
    ws = w.add_sheet('Sheet1')

    # Write data to XLS
    # Print headers
    ws.write(0, 0, 'hogiadinh', bold_style)
    ws.write(0, 1, 'hotenthanhvien', bold_style)
    ws.write(0, 2, 'ngaysinh', bold_style)
    ws.write(0, 3, 'gioitinh', bold_style)
    ws.write(0, 4, 'quequan', bold_style)
    ws.write(0, 5, 'quanhevoichuho', bold_style)
    ws.write(0, 6, 'thuocdoituongbodoixuatngu', bold_style)
    ws.write(0, 7, 'thuocdoituongbaotro', bold_style)
    ws.write(0, 8, 'thuocdoituongxichlo', bold_style)
    ws.write(0, 9, 'cmnd', bold_style)
    ws.write(0, 10, 'thuocdoituongkhuyetat', bold_style)
    ws.write(0, 11, 'thuocdoituongchinhsach', bold_style)
    ws.write(0, 12, 'cobaohiem', bold_style)
    ws.write(0, 13, 'tinhtranghonnhan', bold_style)
    ws.write(0, 14, 'trinhdo', bold_style)
    ws.write(0, 15, 'chuyenmon', bold_style)
    ws.write(0, 16, 'tongiao', bold_style)
    ws.write(0, 17, 'dantoc', bold_style)

    row = 1
    for data in chuho:
        thuocdoituongxichlo = unicode('có', "utf-8") if data.thuocdoituongxichlo else unicode('không', "utf-8")
        cobaohiem = unicode('có', "utf-8") if data.cobaohiem else unicode('không', "utf-8")
        thuocdoituongbodoixuatngu = unicode('có', "utf-8") if data.thuocdoituongbodoixuatngu else unicode('không', "utf-8")

        ws.write(row, 0, data.hogiadinh.masoho, top_align_style)
        ws.write(row, 1, data.hotenthanhvien, top_align_style)
        ws.write(row, 2, data.ngaysinh.strftime("%d/%m/%Y"), top_align_style)
        ws.write(row, 3, data.gioitinh, top_align_style)
        ws.write(row, 4, data.quequan, top_align_style)
        ws.write(row, 5, data.quanhevoichuho, top_align_style)
        ws.write(row, 6, thuocdoituongbodoixuatngu, top_align_style)
        ws.write(row, 7, data.thuocdoituongbaotro, top_align_style)
        ws.write(row, 8, thuocdoituongxichlo, top_align_style)
        ws.write(row, 9, data.cmnd, top_align_style)
        ws.write(row, 10, data.thuocdoituongkhuyetat, top_align_style)
        ws.write(row, 11, data.thuocdoituongchinhsach, top_align_style)
        ws.write(row, 12, cobaohiem, top_align_style)
        ws.write(row, 13, data.tinhtranghonnhan, top_align_style)
        ws.write(row, 14, data.trinhdo, top_align_style)
        ws.write(row, 15, data.chuyenmon, top_align_style)
        ws.write(row, 16, data.tongiao, top_align_style)
        ws.write(row, 17, data.dantoc, top_align_style)
        row += 1

    return w


def export_danhsachhogiadinh():
    hogiadinh = HoGiaDinh.objects.all()

    w = Workbook()
    ws = w.add_sheet('Sheet1')

    # Write data to XLS
    # Print headers
    ws.write(0, 0, 'masoho', bold_style)
    ws.write(0, 1, 'sonha', bold_style)
    ws.write(0, 2, 'todanpho', bold_style)
    ws.write(0, 3, 'tinhtrangcutru', bold_style)
    ws.write(0, 4, 'thuochongheo', bold_style)
    ws.write(0, 5, 'thuochochinhsach', bold_style)
    ws.write(0, 6, 'thuochocodoituongbaotro', bold_style)
    ws.write(0, 7, 'cogiaychungnhanquyensdd', bold_style)
    ws.write(0, 8, 'sodienthoai', bold_style)
    ws.write(0, 9, 'tienthue', bold_style)
    ws.write(0, 10, 'chuho', bold_style)

    row = 1
    for data in hogiadinh:
        thuochochinhsach = unicode('có', "utf-8") if data.thuochochinhsach else unicode('không', "utf-8")
        thuochocodoituongbaotro = unicode('có', "utf-8") if data.thuochocodoituongbaotro else unicode('không', "utf-8")

        ws.write(row, 0, data.masoho, top_align_style)
        ws.write(row, 1, data.sonha, top_align_style)
        ws.write(row, 2, data.todanpho, top_align_style)
        ws.write(row, 3, data.tinhtrangcutru, top_align_style)
        ws.write(row, 4, data.thuochongheo, top_align_style)
        ws.write(row, 5, thuochochinhsach, top_align_style)
        ws.write(row, 6, thuochocodoituongbaotro, top_align_style)
        ws.write(row, 7, data.cogiaychungnhanquyensdd, top_align_style)
        ws.write(row, 8, data.sodienthoai, top_align_style)
        ws.write(row, 9, data.tienthue, top_align_style)
        ws.write(row, 10, data.chuho, top_align_style)
        row += 1

    return w


def export_danhsachthuongbenhbinh():
    thanhvien = ThanhVien.objects.filter(Q(thuocdoituongchinhsach__contains='Thương binh') | Q(thuocdoituongchinhsach__contains='Bệnh binh'))

    w = Workbook()
    ws = w.add_sheet('Sheet1')

    # Write data to XLS
    # Print headers
    ws.write(0, 0, 'hogiadinh', bold_style)
    ws.write(0, 1, 'hotenthanhvien', bold_style)
    ws.write(0, 2, 'ngaysinh', bold_style)
    ws.write(0, 3, 'gioitinh', bold_style)
    ws.write(0, 4, 'quequan', bold_style)
    ws.write(0, 5, 'quanhevoichuho', bold_style)
    ws.write(0, 6, 'thuocdoituongbodoixuatngu', bold_style)
    ws.write(0, 7, 'thuocdoituongbaotro', bold_style)
    ws.write(0, 8, 'thuocdoituongxichlo', bold_style)
    ws.write(0, 9, 'cmnd', bold_style)
    ws.write(0, 10, 'thuocdoituongkhuyetat', bold_style)
    ws.write(0, 11, 'thuocdoituongchinhsach', bold_style)
    ws.write(0, 12, 'cobaohiem', bold_style)
    ws.write(0, 13, 'tinhtranghonnhan', bold_style)
    ws.write(0, 14, 'trinhdo', bold_style)
    ws.write(0, 15, 'chuyenmon', bold_style)
    ws.write(0, 16, 'tongiao', bold_style)
    ws.write(0, 17, 'dantoc', bold_style)

    row = 1
    for data in thanhvien:
        thuocdoituongxichlo = unicode('có', "utf-8") if data.thuocdoituongxichlo else unicode('không', "utf-8")
        cobaohiem = unicode('có', "utf-8") if data.cobaohiem else unicode('không', "utf-8")
        thuocdoituongbodoixuatngu = unicode('có', "utf-8") if data.thuocdoituongbodoixuatngu else unicode('không', "utf-8")

        ws.write(row, 0, data.hogiadinh.masoho, top_align_style)
        ws.write(row, 1, data.hotenthanhvien, top_align_style)
        ws.write(row, 2, data.ngaysinh.strftime("%d/%m/%Y"), top_align_style)
        ws.write(row, 3, data.gioitinh, top_align_style)
        ws.write(row, 4, data.quequan, top_align_style)
        ws.write(row, 5, data.quanhevoichuho, top_align_style)
        ws.write(row, 6, thuocdoituongbodoixuatngu, top_align_style)
        ws.write(row, 7, data.thuocdoituongbaotro, top_align_style)
        ws.write(row, 8, thuocdoituongxichlo, top_align_style)
        ws.write(row, 9, data.cmnd, top_align_style)
        ws.write(row, 10, data.thuocdoituongkhuyetat, top_align_style)
        ws.write(row, 11, data.thuocdoituongchinhsach, top_align_style)
        ws.write(row, 12, cobaohiem, top_align_style)
        ws.write(row, 13, data.tinhtranghonnhan, top_align_style)
        ws.write(row, 14, data.trinhdo, top_align_style)
        ws.write(row, 15, data.chuyenmon, top_align_style)
        ws.write(row, 16, data.tongiao, top_align_style)
        ws.write(row, 17, data.dantoc, top_align_style)
        row += 1

    return w


def export_danhsachhongheo():
    hogiadinh = HoGiaDinh.objects.filter(~Q(thuochongheo = ''))

    w = Workbook()
    ws = w.add_sheet('Sheet1')

    # Write data to XLS
    # Print headers
    ws.write(0, 0, 'masoho', bold_style)
    ws.write(0, 1, 'sonha', bold_style)
    ws.write(0, 2, 'todanpho', bold_style)
    ws.write(0, 3, 'tinhtrangcutru', bold_style)
    ws.write(0, 4, 'thuochongheo', bold_style)
    ws.write(0, 5, 'thuochochinhsach', bold_style)
    ws.write(0, 6, 'thuochocodoituongbaotro', bold_style)
    ws.write(0, 7, 'cogiaychungnhanquyensdd', bold_style)
    ws.write(0, 8, 'sodienthoai', bold_style)
    ws.write(0, 9, 'tienthue', bold_style)
    ws.write(0, 10, 'chuho', bold_style)

    row = 1
    for data in hogiadinh:
        thuochochinhsach = unicode('có', "utf-8") if data.thuochochinhsach else unicode('không', "utf-8")
        thuochocodoituongbaotro = unicode('có', "utf-8") if data.thuochocodoituongbaotro else unicode('không', "utf-8")

        ws.write(row, 0, data.masoho, top_align_style)
        ws.write(row, 1, data.sonha, top_align_style)
        ws.write(row, 2, data.todanpho, top_align_style)
        ws.write(row, 3, data.tinhtrangcutru, top_align_style)
        ws.write(row, 4, data.thuochongheo, top_align_style)
        ws.write(row, 5, thuochochinhsach, top_align_style)
        ws.write(row, 6, thuochocodoituongbaotro, top_align_style)
        ws.write(row, 7, data.cogiaychungnhanquyensdd, top_align_style)
        ws.write(row, 8, data.sodienthoai, top_align_style)
        ws.write(row, 9, data.tienthue, top_align_style)
        ws.write(row, 10, data.chuho, top_align_style)
        row += 1

    return w


def export_danhsachtreemduoi6():
    thanhvien = ThanhVien.objects.all()
    
    thanhvienObj = []
    for mem in thanhvien:
        tuoi = calculate_age(mem.ngaysinh)
        if tuoi <= 6:
            thanhvienObj.append(mem)
    thanhvien = thanhvienObj

    w = Workbook()
    ws = w.add_sheet('Sheet1')

    # Write data to XLS
    # Print headers
    ws.write(0, 0, 'hogiadinh', bold_style)
    ws.write(0, 1, 'hotenthanhvien', bold_style)
    ws.write(0, 2, 'ngaysinh', bold_style)
    ws.write(0, 3, 'gioitinh', bold_style)
    ws.write(0, 4, 'quequan', bold_style)
    ws.write(0, 5, 'quanhevoichuho', bold_style)
    ws.write(0, 6, 'thuocdoituongbodoixuatngu', bold_style)
    ws.write(0, 7, 'thuocdoituongbaotro', bold_style)
    ws.write(0, 8, 'thuocdoituongxichlo', bold_style)
    ws.write(0, 9, 'cmnd', bold_style)
    ws.write(0, 10, 'thuocdoituongkhuyetat', bold_style)
    ws.write(0, 11, 'thuocdoituongchinhsach', bold_style)
    ws.write(0, 12, 'cobaohiem', bold_style)
    ws.write(0, 13, 'tinhtranghonnhan', bold_style)
    ws.write(0, 14, 'trinhdo', bold_style)
    ws.write(0, 15, 'chuyenmon', bold_style)
    ws.write(0, 16, 'tongiao', bold_style)
    ws.write(0, 17, 'dantoc', bold_style)

    row = 1
    for data in thanhvien:
        thuocdoituongxichlo = unicode('có', "utf-8") if data.thuocdoituongxichlo else unicode('không', "utf-8")
        cobaohiem = unicode('có', "utf-8") if data.cobaohiem else unicode('không', "utf-8")
        thuocdoituongbodoixuatngu = unicode('có', "utf-8") if data.thuocdoituongbodoixuatngu else unicode('không', "utf-8")

        ws.write(row, 0, data.hogiadinh.masoho, top_align_style)
        ws.write(row, 1, data.hotenthanhvien, top_align_style)
        ws.write(row, 2, data.ngaysinh.strftime("%d/%m/%Y"), top_align_style)
        ws.write(row, 3, data.gioitinh, top_align_style)
        ws.write(row, 4, data.quequan, top_align_style)
        ws.write(row, 5, data.quanhevoichuho, top_align_style)
        ws.write(row, 6, thuocdoituongbodoixuatngu, top_align_style)
        ws.write(row, 7, data.thuocdoituongbaotro, top_align_style)
        ws.write(row, 8, thuocdoituongxichlo, top_align_style)
        ws.write(row, 9, data.cmnd, top_align_style)
        ws.write(row, 10, data.thuocdoituongkhuyetat, top_align_style)
        ws.write(row, 11, data.thuocdoituongchinhsach, top_align_style)
        ws.write(row, 12, cobaohiem, top_align_style)
        ws.write(row, 13, data.tinhtranghonnhan, top_align_style)
        ws.write(row, 14, data.trinhdo, top_align_style)
        ws.write(row, 15, data.chuyenmon, top_align_style)
        ws.write(row, 16, data.tongiao, top_align_style)
        ws.write(row, 17, data.dantoc, top_align_style)
        row += 1

    return w


def export_danhsachdoituongchinhsach():
    thanhvien = ThanhVien.objects.filter(~Q(thuocdoituongchinhsach = ''))

    w = Workbook()
    ws = w.add_sheet('Sheet1')

    # Write data to XLS
    # Print headers
    ws.write(0, 0, 'hogiadinh', bold_style)
    ws.write(0, 1, 'hotenthanhvien', bold_style)
    ws.write(0, 2, 'ngaysinh', bold_style)
    ws.write(0, 3, 'gioitinh', bold_style)
    ws.write(0, 4, 'quequan', bold_style)
    ws.write(0, 5, 'quanhevoichuho', bold_style)
    ws.write(0, 6, 'thuocdoituongbodoixuatngu', bold_style)
    ws.write(0, 7, 'thuocdoituongbaotro', bold_style)
    ws.write(0, 8, 'thuocdoituongxichlo', bold_style)
    ws.write(0, 9, 'cmnd', bold_style)
    ws.write(0, 10, 'thuocdoituongkhuyetat', bold_style)
    ws.write(0, 11, 'thuocdoituongchinhsach', bold_style)
    ws.write(0, 12, 'cobaohiem', bold_style)
    ws.write(0, 13, 'tinhtranghonnhan', bold_style)
    ws.write(0, 14, 'trinhdo', bold_style)
    ws.write(0, 15, 'chuyenmon', bold_style)
    ws.write(0, 16, 'tongiao', bold_style)
    ws.write(0, 17, 'dantoc', bold_style)

    row = 1
    for data in thanhvien:
        thuocdoituongxichlo = unicode('có', "utf-8") if data.thuocdoituongxichlo else unicode('không', "utf-8")
        cobaohiem = unicode('có', "utf-8") if data.cobaohiem else unicode('không', "utf-8")
        thuocdoituongbodoixuatngu = unicode('có', "utf-8") if data.thuocdoituongbodoixuatngu else unicode('không', "utf-8")

        ws.write(row, 0, data.hogiadinh.masoho, top_align_style)
        ws.write(row, 1, data.hotenthanhvien, top_align_style)
        ws.write(row, 2, data.ngaysinh.strftime("%d/%m/%Y"), top_align_style)
        ws.write(row, 3, data.gioitinh, top_align_style)
        ws.write(row, 4, data.quequan, top_align_style)
        ws.write(row, 5, data.quanhevoichuho, top_align_style)
        ws.write(row, 6, thuocdoituongbodoixuatngu, top_align_style)
        ws.write(row, 7, data.thuocdoituongbaotro, top_align_style)
        ws.write(row, 8, thuocdoituongxichlo, top_align_style)
        ws.write(row, 9, data.cmnd, top_align_style)
        ws.write(row, 10, data.thuocdoituongkhuyetat, top_align_style)
        ws.write(row, 11, data.thuocdoituongchinhsach, top_align_style)
        ws.write(row, 12, cobaohiem, top_align_style)
        ws.write(row, 13, data.tinhtranghonnhan, top_align_style)
        ws.write(row, 14, data.trinhdo, top_align_style)
        ws.write(row, 15, data.chuyenmon, top_align_style)
        ws.write(row, 16, data.tongiao, top_align_style)
        ws.write(row, 17, data.dantoc, top_align_style)
        row += 1

    return w


def export_danhsachdoituongbaotro():
    thanhvien = ThanhVien.objects.filter(~Q(thuocdoituongbaotro = ''))

    w = Workbook()
    ws = w.add_sheet('Sheet1')

    # Write data to XLS
    # Print headers
    ws.write(0, 0, 'hogiadinh', bold_style)
    ws.write(0, 1, 'hotenthanhvien', bold_style)
    ws.write(0, 2, 'ngaysinh', bold_style)
    ws.write(0, 3, 'gioitinh', bold_style)
    ws.write(0, 4, 'quequan', bold_style)
    ws.write(0, 5, 'quanhevoichuho', bold_style)
    ws.write(0, 6, 'thuocdoituongbodoixuatngu', bold_style)
    ws.write(0, 7, 'thuocdoituongbaotro', bold_style)
    ws.write(0, 8, 'thuocdoituongxichlo', bold_style)
    ws.write(0, 9, 'cmnd', bold_style)
    ws.write(0, 10, 'thuocdoituongkhuyetat', bold_style)
    ws.write(0, 11, 'thuocdoituongchinhsach', bold_style)
    ws.write(0, 12, 'cobaohiem', bold_style)
    ws.write(0, 13, 'tinhtranghonnhan', bold_style)
    ws.write(0, 14, 'trinhdo', bold_style)
    ws.write(0, 15, 'chuyenmon', bold_style)
    ws.write(0, 16, 'tongiao', bold_style)
    ws.write(0, 17, 'dantoc', bold_style)

    row = 1
    for data in thanhvien:
        thuocdoituongxichlo = unicode('có', "utf-8") if data.thuocdoituongxichlo else unicode('không', "utf-8")
        cobaohiem = unicode('có', "utf-8") if data.cobaohiem else unicode('không', "utf-8")
        thuocdoituongbodoixuatngu = unicode('có', "utf-8") if data.thuocdoituongbodoixuatngu else unicode('không', "utf-8")

        ws.write(row, 0, data.hogiadinh.masoho, top_align_style)
        ws.write(row, 1, data.hotenthanhvien, top_align_style)
        ws.write(row, 2, data.ngaysinh.strftime("%d/%m/%Y"), top_align_style)
        ws.write(row, 3, data.gioitinh, top_align_style)
        ws.write(row, 4, data.quequan, top_align_style)
        ws.write(row, 5, data.quanhevoichuho, top_align_style)
        ws.write(row, 6, thuocdoituongbodoixuatngu, top_align_style)
        ws.write(row, 7, data.thuocdoituongbaotro, top_align_style)
        ws.write(row, 8, thuocdoituongxichlo, top_align_style)
        ws.write(row, 9, data.cmnd, top_align_style)
        ws.write(row, 10, data.thuocdoituongkhuyetat, top_align_style)
        ws.write(row, 11, data.thuocdoituongchinhsach, top_align_style)
        ws.write(row, 12, cobaohiem, top_align_style)
        ws.write(row, 13, data.tinhtranghonnhan, top_align_style)
        ws.write(row, 14, data.trinhdo, top_align_style)
        ws.write(row, 15, data.chuyenmon, top_align_style)
        ws.write(row, 16, data.tongiao, top_align_style)
        ws.write(row, 17, data.dantoc, top_align_style)
        row += 1

    return w


def export_danhsachdoituongbodoixuatngu():
    thanhvien = ThanhVien.objects.filter(thuocdoituongbodoixuatngu=True)

    w = Workbook()
    ws = w.add_sheet('Sheet1')

    # Write data to XLS
    # Print headers
    ws.write(0, 0, 'hogiadinh', bold_style)
    ws.write(0, 1, 'hotenthanhvien', bold_style)
    ws.write(0, 2, 'ngaysinh', bold_style)
    ws.write(0, 3, 'gioitinh', bold_style)
    ws.write(0, 4, 'quequan', bold_style)
    ws.write(0, 5, 'quanhevoichuho', bold_style)
    ws.write(0, 6, 'thuocdoituongbodoixuatngu', bold_style)
    ws.write(0, 7, 'thuocdoituongbaotro', bold_style)
    ws.write(0, 8, 'thuocdoituongxichlo', bold_style)
    ws.write(0, 9, 'cmnd', bold_style)
    ws.write(0, 10, 'thuocdoituongkhuyetat', bold_style)
    ws.write(0, 11, 'thuocdoituongchinhsach', bold_style)
    ws.write(0, 12, 'cobaohiem', bold_style)
    ws.write(0, 13, 'tinhtranghonnhan', bold_style)
    ws.write(0, 14, 'trinhdo', bold_style)
    ws.write(0, 15, 'chuyenmon', bold_style)
    ws.write(0, 16, 'tongiao', bold_style)
    ws.write(0, 17, 'dantoc', bold_style)

    row = 1
    for data in thanhvien:
        thuocdoituongxichlo = unicode('có', "utf-8") if data.thuocdoituongxichlo else unicode('không', "utf-8")
        cobaohiem = unicode('có', "utf-8") if data.cobaohiem else unicode('không', "utf-8")
        thuocdoituongbodoixuatngu = unicode('có', "utf-8") if data.thuocdoituongbodoixuatngu else unicode('không', "utf-8")

        ws.write(row, 0, data.hogiadinh.masoho, top_align_style)
        ws.write(row, 1, data.hotenthanhvien, top_align_style)
        ws.write(row, 2, data.ngaysinh.strftime("%d/%m/%Y"), top_align_style)
        ws.write(row, 3, data.gioitinh, top_align_style)
        ws.write(row, 4, data.quequan, top_align_style)
        ws.write(row, 5, data.quanhevoichuho, top_align_style)
        ws.write(row, 6, thuocdoituongbodoixuatngu, top_align_style)
        ws.write(row, 7, data.thuocdoituongbaotro, top_align_style)
        ws.write(row, 8, thuocdoituongxichlo, top_align_style)
        ws.write(row, 9, data.cmnd, top_align_style)
        ws.write(row, 10, data.thuocdoituongkhuyetat, top_align_style)
        ws.write(row, 11, data.thuocdoituongchinhsach, top_align_style)
        ws.write(row, 12, cobaohiem, top_align_style)
        ws.write(row, 13, data.tinhtranghonnhan, top_align_style)
        ws.write(row, 14, data.trinhdo, top_align_style)
        ws.write(row, 15, data.chuyenmon, top_align_style)
        ws.write(row, 16, data.tongiao, top_align_style)
        ws.write(row, 17, data.dantoc, top_align_style)
        row += 1

    return w

def export_timkiemthanhvien(thanhvien):
    w = Workbook()
    ws = w.add_sheet('Sheet1')

    # Write data to XLS
    # Print headers
    ws.write(0, 0, 'hogiadinh', bold_style)
    ws.write(0, 1, 'hotenthanhvien', bold_style)
    ws.write(0, 2, 'ngaysinh', bold_style)
    ws.write(0, 3, 'gioitinh', bold_style)
    ws.write(0, 4, 'quequan', bold_style)
    ws.write(0, 5, 'quanhevoichuho', bold_style)
    ws.write(0, 6, 'thuocdoituongbodoixuatngu', bold_style)
    ws.write(0, 7, 'thuocdoituongbaotro', bold_style)
    ws.write(0, 8, 'thuocdoituongxichlo', bold_style)
    ws.write(0, 9, 'cmnd', bold_style)
    ws.write(0, 10, 'thuocdoituongkhuyetat', bold_style)
    ws.write(0, 11, 'thuocdoituongchinhsach', bold_style)
    ws.write(0, 12, 'cobaohiem', bold_style)
    ws.write(0, 13, 'tinhtranghonnhan', bold_style)
    ws.write(0, 14, 'trinhdo', bold_style)
    ws.write(0, 15, 'chuyenmon', bold_style)
    ws.write(0, 16, 'tongiao', bold_style)
    ws.write(0, 17, 'dantoc', bold_style)

    row = 1
    for data in thanhvien:
        thuocdoituongxichlo = unicode('có', "utf-8") if data.thuocdoituongxichlo else unicode('không', "utf-8")
        cobaohiem = unicode('có', "utf-8") if data.cobaohiem else unicode('không', "utf-8")
        thuocdoituongbodoixuatngu = unicode('có', "utf-8") if data.thuocdoituongbodoixuatngu else unicode('không', "utf-8")

        ws.write(row, 0, data.hogiadinh.masoho, top_align_style)
        ws.write(row, 1, data.hotenthanhvien, top_align_style)
        ws.write(row, 2, data.ngaysinh.strftime("%d/%m/%Y"), top_align_style)
        ws.write(row, 3, data.gioitinh, top_align_style)
        ws.write(row, 4, data.quequan, top_align_style)
        ws.write(row, 5, data.quanhevoichuho, top_align_style)
        ws.write(row, 6, thuocdoituongbodoixuatngu, top_align_style)
        ws.write(row, 7, data.thuocdoituongbaotro, top_align_style)
        ws.write(row, 8, thuocdoituongxichlo, top_align_style)
        ws.write(row, 9, data.cmnd, top_align_style)
        ws.write(row, 10, data.thuocdoituongkhuyetat, top_align_style)
        ws.write(row, 11, data.thuocdoituongchinhsach, top_align_style)
        ws.write(row, 12, cobaohiem, top_align_style)
        ws.write(row, 13, data.tinhtranghonnhan, top_align_style)
        ws.write(row, 14, data.trinhdo, top_align_style)
        ws.write(row, 15, data.chuyenmon, top_align_style)
        ws.write(row, 16, data.tongiao, top_align_style)
        ws.write(row, 17, data.dantoc, top_align_style)
        row += 1

    return w

def export_timkiemhogiadinh(hogiadinh):
    w = Workbook()
    ws = w.add_sheet('Sheet1')

    # Write data to XLS
    # Print headers
    ws.write(0, 0, 'masoho', bold_style)
    ws.write(0, 1, 'sonha', bold_style)
    ws.write(0, 2, 'todanpho', bold_style)
    ws.write(0, 3, 'tinhtrangcutru', bold_style)
    ws.write(0, 4, 'thuochongheo', bold_style)
    ws.write(0, 5, 'thuochochinhsach', bold_style)
    ws.write(0, 6, 'thuochocodoituongbaotro', bold_style)
    ws.write(0, 7, 'cogiaychungnhanquyensdd', bold_style)
    ws.write(0, 8, 'sodienthoai', bold_style)
    ws.write(0, 9, 'tienthue', bold_style)
    ws.write(0, 10, 'chuho', bold_style)

    row = 1
    for data in hogiadinh:
        thuochochinhsach = unicode('có', "utf-8") if data.thuochochinhsach else unicode('không', "utf-8")
        thuochocodoituongbaotro = unicode('có', "utf-8") if data.thuochocodoituongbaotro else unicode('không', "utf-8")

        ws.write(row, 0, data.masoho, top_align_style)
        ws.write(row, 1, data.sonha, top_align_style)
        ws.write(row, 2, data.todanpho, top_align_style)
        ws.write(row, 3, data.tinhtrangcutru, top_align_style)
        ws.write(row, 4, data.thuochongheo, top_align_style)
        ws.write(row, 5, thuochochinhsach, top_align_style)
        ws.write(row, 6, thuochocodoituongbaotro, top_align_style)
        ws.write(row, 7, data.cogiaychungnhanquyensdd, top_align_style)
        ws.write(row, 8, data.sodienthoai, top_align_style)
        ws.write(row, 9, data.tienthue, top_align_style)
        ws.write(row, 10, data.chuho, top_align_style)
        row += 1

    return w