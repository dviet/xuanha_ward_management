#!/usr/bin/env python
# -*- coding: utf-8 -*- 
import xlrd, xlwt
from datetime import date
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.views import redirect_to_login
from django.conf import settings
from django.contrib.auth.models import User
from forms import LoginForm, HoGiaDinhAddForm, ThanhVienAddForm, UploadFileForm,\
NguoidungForm, PhanQuyenForm, HoGiaDinhEditForm, ThanhVienEditForm, TimKiemHoForm,\
TimKiemThanhVienForm
from models import PhanQuyen, NguoiDung, HoGiaDinh, ThanhVien
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login as django_login, logout as django_logout
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from content import PERMISSION_FIELDS
from django.contrib.auth.decorators import user_passes_test
import logging
from utils import *

logging_options = dict(
        level=logging.INFO,
        format='%(asctime)s %(levelname)-8s %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S',
        filename='log.txt',
    )
logging.basicConfig(**logging_options)

@login_required
def index(request):
    if request.method == 'POST':
        if 'masoho' in request.POST:
            masoho_list = dict(request.POST.iterlists())['masoho']
            for maso in masoho_list:
                print maso
                giadinh = HoGiaDinh.objects.get(id=int(maso))
                for thanhvien in giadinh.thanhvien_set.all():
                    thanhvien.delete()
                giadinh.delete()
                logging.info('-' * 60)
                logging.info(u'%s đã xóa hộ gia đình %s' %(request.user.get_profile().tennguoidung, giadinh.masoho))

    hogiadinh = HoGiaDinh.objects.all()
    masohoList = []
    for ho in hogiadinh:
        masohoList.append(ho.masoho)

    search = ''
    if request.method == 'GET':
        if request.GET.get("search_masoho"):
            search = search + '&search_masoho=' + request.GET.get("search_masoho")
            hogiadinh = hogiadinh.filter(masoho=request.GET.get("search_masoho"))
        if request.GET.get("search_chuho"):
            search = search + '&search_chuho=' + request.GET.get("search_chuho")
            chu = ThanhVien.objects.filter(hotenthanhvien__contains=request.GET.get("search_chuho"))
            if chu:
                hogiadinh = hogiadinh.filter(chuho__in=chu)
    hogiadinhList = []

    for ho in hogiadinh:
        chuho = ThanhVien.objects.get(id=ho.chuho)
        hogiadinhObj = {
                        'masoho': ho.masoho,
                        'id': ho.id,
                        'todanpho': ho.todanpho,
                        'tenchuho': chuho.hotenthanhvien,
                        'ngaysinh': chuho.ngaysinh,
                        'gioitinh': chuho.gioitinh,
                        'sonha': ho.sonha,
                        'sodienthoai': ho.sodienthoai,
                        'tinhtrangcutru': ho.tinhtrangcutru,
                        'thuochongheo': ho.thuochongheo,
                        'thuochochinhsach': ho.thuochochinhsach,
                        'thuocdoituongbaotro': ho.thuochocodoituongbaotro,
                        }
        hogiadinhList.append(hogiadinhObj)

    paginator = Paginator(hogiadinhList, 5)
    page = request.GET.get('page')
    try:
        hogiadinhList = paginator.page(page)
    except PageNotAnInteger:
        hogiadinhList = paginator.page(1)
    except EmptyPage:
        hogiadinhList = paginator.page(paginator.num_pages)
    context = {'hogiadinh': hogiadinhList, 'masoho': masohoList, 'search': search, 'hoPage': 'active'}
    return render_to_response('index.html', context, context_instance=RequestContext(request))

def login(request):
    next = request.GET.get("next",settings.DASHBOARD_URL)
    form = LoginForm()
    error = None
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            user = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password'])
            if user is not None:
                if user.is_active:
                    django_login(request, user)
                    #logging.info('-' * 60)
                    #logging.info(u'%s đã đăng nhập.' %(request.user.get_profile().tennguoidung))
                    return HttpResponseRedirect('/')
                else:
                    error = "Tài khoản chưa được kích hoạt."
            else:
                error = "Tên đăng nhập hoặc mật khẩu không đúng. Làm ơn kiểm tra lại."
        else:
            error = "Tên đăng nhập hoặc mật khẩu không hợp lệ."

    return render_to_response("login.html",{'form':form, 'error':error},context_instance=RequestContext(request))

@login_required
def hogiadinh(request, masoho):
    if request.method == 'POST':
        if 'member_id' in request.POST:
            member_list = dict(request.POST.iterlists())['member_id']
            for member in member_list:
                thanhvien = ThanhVien.objects.get(id=int(member))
                thanhvien.delete()
                logging.info('-' * 60)
                logging.info(u'%s đã xóa thành viên %s của hộ gia đình %s' %(request.user.get_profile().tennguoidung, thanhvien.hotenthanhvien, thanhvien.hogiadinh.masoho))
    hogiadinh = HoGiaDinh.objects.get(masoho=masoho)
    chuho = ThanhVien.objects.get(id=hogiadinh.chuho)
    context = {'hogiadinh': hogiadinh, 'chuho': chuho, 'hoPage': 'active'}
    return render_to_response('hogiadinh.html', context, context_instance=RequestContext(request))

def logout(request, next_page=None):
    django_logout(request)
    return HttpResponseRedirect(next_page)

@login_required
def themhogiadinh(request):
    form_giadinh = HoGiaDinhAddForm()
    form_thanhvien = ThanhVienAddForm()
    if request.method == "POST":
        form_giadinh = HoGiaDinhAddForm(request.POST)
        form_thanhvien = ThanhVienAddForm(request.POST)
        if form_giadinh.is_valid() and form_thanhvien.is_valid():
            giadinh = form_giadinh.save()
            thanhvien = form_thanhvien.save(commit=False)
            thanhvien.hogiadinh = giadinh
            thanhvien.save()
            giadinh.chuho = thanhvien.id
            giadinh.save()
            logging.info('-' * 60)
            logging.info(u'%s đã thêm hộ gia đình %s' %(request.user.get_profile().tennguoidung, giadinh.masoho))
            return HttpResponseRedirect('/hogiadinh/'+giadinh.masoho)
    context = {'form_giadinh': form_giadinh, 'form_thanhvien':form_thanhvien, 'hoPage': 'active'}
    return render_to_response('themhogiadinh.html', context, context_instance=RequestContext(request))

@login_required
def suahogiadinh(request, masoho):
    hogiadinh = HoGiaDinh.objects.get(masoho=masoho)
    form_giadinh = HoGiaDinhEditForm(instance=hogiadinh)
    if request.method == "POST":
        form_giadinh = HoGiaDinhEditForm(request.POST, instance=hogiadinh)
        if form_giadinh.is_valid():
            hogiadinh = form_giadinh.save(hogiadinh)
            logging.info('-' * 60)
            logging.info(u'%s đã chỉnh sửa hộ gia đình %s' %(request.user.get_profile().tennguoidung, hogiadinh.masoho))
            return HttpResponseRedirect('/hogiadinh/'+hogiadinh.masoho)
    context = {'form_giadinh': form_giadinh, 'hoPage': 'active'}
    return render_to_response('suahogiadinh.html', context, context_instance=RequestContext(request))

@login_required
def themthanhvien(request, masoho):
    form = ThanhVienAddForm(initial={'gioitinh': 'Nam'})
    if request.method == "POST":
        form = ThanhVienAddForm(request.POST)
        if form.is_valid():
            hogiadinh = HoGiaDinh.objects.get(masoho=masoho)
            thanhvien = form.save(commit=False)
            thanhvien.hogiadinh = hogiadinh
            #thanhvien.save(commit=False)
            thanhvien.thuocdoituongchinhsach = form.cleaned_data['thuocdoituongchinhsach']
            thanhvien.thuocdoituongbaotro = form.cleaned_data['thuocdoituongbaotro']
            thanhvien.save()
            logging.info('-' * 60)
            logging.info(u'%s đã thêm thành viên %s vào hộ gia đình %s' %(request.user.get_profile().tennguoidung, thanhvien.hotenthanhvien, thanhvien.hogiadinh.masoho))
            return HttpResponseRedirect('/hogiadinh/'+hogiadinh.masoho)
    context = {'form': form, 'hoPage': 'active'}
    return render_to_response('themthanhvien.html', context, context_instance=RequestContext(request))

@login_required
def suathanhvien(request, mathanhvien):
    thanhvien = ThanhVien.objects.get(id=int(mathanhvien))
    init_data = {
                 'thuocdoituongchinhsach':thanhvien.thuocdoituongchinhsach.split(';'),
                 'thuocdoituongbaotro':thanhvien.thuocdoituongbaotro.split(';')
                 }
    form_thanhvien = ThanhVienEditForm(instance=thanhvien, user=request.user, initial=init_data)
    if request.method == "POST":
        form_thanhvien = ThanhVienEditForm(request.POST, instance=thanhvien, user=request.user)
        if form_thanhvien.is_valid():
            thanhvien = form_thanhvien.save(commit=False)
            thanhvien.thuocdoituongchinhsach = form_thanhvien.cleaned_data['thuocdoituongchinhsach']
            thanhvien.thuocdoituongbaotro = form_thanhvien.cleaned_data['thuocdoituongbaotro']
            thanhvien.save()
            logging.info('-' * 60)
            logging.info(u'%s đã chỉnh sửa thành viên %s của hộ gia đình %s' %(request.user.get_profile().tennguoidung, thanhvien.hotenthanhvien, thanhvien.hogiadinh.masoho))
            return HttpResponseRedirect('/hogiadinh/' + thanhvien.hogiadinh.masoho)
    context = {'form_thanhvien':form_thanhvien, 'hoPage': 'active'}
    return render_to_response('suathanhvien.html', context, context_instance=RequestContext(request))

def timkiemhogiadinh(request):
    hogiadinh = HoGiaDinh.objects.all()
    hoList = []
    for ho in hogiadinh:
        hoList.append(ho.masoho)
    context = {'hogiadinhList': hoList, 'timkiemPage': 'active'}
    form_ho = TimKiemHoForm()
    context.update({'form_ho': form_ho})

    hogiadinhList = []
    search = ''
    if request.method == 'GET':
        thuochochinhsach = convert_bool(request.GET.get("thuochochinhsach"))
        thuochobaotro = convert_bool(request.GET.get("thuochobaotro"))
        if request.GET.get("orderby"):
            hogiadinh = hogiadinh.order_by('todanpho')
            orderby = '&orderby=' + request.GET.get("orderby")
            context.update({'orderby': orderby})
        if request.GET.get("masoho"):
            search = search + '&masoho=' + request.GET.get("masoho")
            hogiadinh = hogiadinh.filter(masoho__contains=request.GET.get("masoho"))
        if request.GET.get("todanpho"):
            search = search + '&todanpho=' + request.GET.get("todanpho")
            hogiadinh = hogiadinh.filter(todanpho=request.GET.get("todanpho"))
        if thuochochinhsach is not None:
            search = search + '&thuochochinhsach=' + str(thuochochinhsach)
            hogiadinh = hogiadinh.filter(thuochochinhsach=thuochochinhsach)
        if thuochobaotro is not None:
            search = search + '&thuochocodoituongbaotro=' + str(thuochobaotro)
            hogiadinh = hogiadinh.filter(thuochocodoituongbaotro=thuochobaotro)
        if request.GET.get("tinhtrangcutru"):
            search = search + '&tinhtrangcutru=' + request.GET.get("tinhtrangcutru")
            hogiadinh = hogiadinh.filter(tinhtrangcutru=request.GET.get("tinhtrangcutru"))
        if request.GET.get("thuochongheo"):
            search = search + '&thuochongheo=' + request.GET.get("thuochongheo")
            hogiadinh = hogiadinh.filter(thuochongheo=request.GET.get("thuochongheo"))
            #hogiadinh = hogiadinh.filter(thuochongheo=True)
        form_ho = TimKiemHoForm(request.GET)
        request.session['hogiadinh'] = hogiadinh

        if not search == '':
            for ho in hogiadinh:
                chuho = ThanhVien.objects.get(id=ho.chuho)
                hogiadinhObj = {
                        'masoho': ho.masoho,
                        'todanpho': ho.todanpho,
                        'tenchuho': chuho.hotenthanhvien,
                        'ngaysinh': chuho.ngaysinh,
                        'gioitinh': chuho.gioitinh,
                        'sonha': ho.sonha,
                        'sodienthoai': ho.sodienthoai,
                        'tinhtrangcutru': ho.tinhtrangcutru,
                        'thuochongheo': ho.thuochongheo,
                        'thuochochinhsach': ho.thuochochinhsach,
                        'thuocdoituongbaotro': ho.thuochocodoituongbaotro,
                        }
                hogiadinhList.append(hogiadinhObj)

    paginator = Paginator(hogiadinhList, 100)
    page = request.GET.get('page')
    try:
        hogiadinhList = paginator.page(page)
    except PageNotAnInteger:
        hogiadinhList = paginator.page(1)
    except EmptyPage:
        hogiadinhList = paginator.page(paginator.num_pages)
    context.update({'hogiadinh': hogiadinhList, 'form_ho': form_ho, 'search': search})
    if not hogiadinhList:
        context.update({'message': 'Không có kết quả nào'})

    return render_to_response('timkiem/timkiemhogiadinh.html', context, context_instance=RequestContext(request))

def xuattimkiemhogiadinh(request):
    import xls_export
    if 'hogiadinh' in request.session:
        hogiadinh = request.session['hogiadinh']
    else:
        hogiadinh = []
    workbook = xls_export.export_timkiemhogiadinh(hogiadinh)

    response = HttpResponse(content_type='application/xls')
    response['Content-Disposition'] = 'attachment; filename="danhsachhogiadinh.xls"'

    workbook.save(response)

    return response

def timkiemthanhvien(request):
    hogiadinh = HoGiaDinh.objects.all()
    hoList = []
    for ho in hogiadinh:
        hoList.append(ho.masoho)
    context = {'hogiadinhList': hoList, 'timkiemPage': 'active'}
    form_thanhvien = TimKiemThanhVienForm()
    context.update({'form_thanhvien': form_thanhvien})

    search = ''
    if request.method == 'POST':
        form_thanhvien = TimKiemThanhVienForm(request.POST)
        form_thanhvien.is_valid()
        thanhvien = ThanhVien.objects.all()
        if request.POST.get("orderby"):
            hogiadinh = hogiadinh.order_by('todanpho')
            orderby = '&orderby=' + request.POST.get("orderby")
            context.update({'orderby': orderby})
        if request.POST.get("masoho"):
            search = search + '&masoho=' + request.POST.get("masoho")
            thanhvien = thanhvien.filter(hogiadinh__masoho=request.POST.get("masoho"))
        if request.POST.get("hotenthanhvien"):
            search = search + '&hotenthanhvien=' + request.POST.get("hotenthanhvien")
            thanhvien = thanhvien.filter(hotenthanhvien__contains=request.POST.get("hotenthanhvien"))
        if request.POST.get("todanpho"):
            search = search + '&todanpho=' + request.POST.get("todanpho")
            thanhvien = thanhvien.filter(hogiadinh__todanpho=request.POST.get("todanpho"))
        if len(form_thanhvien.cleaned_data["thuocdoituongchinhsach"]) >0:
            if form_thanhvien.cleaned_data["thuocdoituongchinhsach"][0] != '':
                css = ""
                for chinhsach in form_thanhvien.cleaned_data["thuocdoituongchinhsach"]:
                    search = search + '&thuocdoituongchinhsach=' + chinhsach
                    css = css + chinhsach +";"
                thanhvien = thanhvien.filter(thuocdoituongchinhsach__icontains=css)
        if len(form_thanhvien.cleaned_data["thuocdoituongbaotro"]) >0:
            if form_thanhvien.cleaned_data["thuocdoituongbaotro"][0] != '':
                bts = ""
                for baotro in form_thanhvien.cleaned_data["thuocdoituongbaotro"]:
                    search = search + '&thuocdoituongbaotro=' + baotro
                    bts = bts + baotro +";"
                thanhvien = thanhvien.filter(thuocdoituongbaotro__icontains=bts)
        if request.POST.get("baohiem"):
            search = search + '&baohiem=' + request.POST.get("baohiem")
            if request.POST.get("baohiem") == '1':
                thanhvien = thanhvien.filter(cobaohiem=True)
            else:
                thanhvien = thanhvien.filter(cobaohiem=False)
        if request.POST.get("tinhtrangcutru"):
            print request.POST.get("tinhtrangcutru")
            search = search + '&tinhtrangcutru=' + request.POST.get("tinhtrangcutru")
            thanhvien = thanhvien.filter(hogiadinh__tinhtrangcutru=request.POST.get("tinhtrangcutru"))
        if request.POST.get("thuocdoituongxichlo"):
            search = search + '&thuocdoituongxichlo=' + request.POST.get("thuocdoituongxichlo")
            if request.POST.get("thuocdoituongxichlo") == '1':
                thanhvien = thanhvien.filter(thuocdoituongxichlo=True)
            else:
                thanhvien = thanhvien.filter(thuocdoituongxichlo=False)
        if request.POST.get("thuocdoituongkhuyetat"):
            search = search + '&thuocdoituongkhuyetat=' + request.POST.get("thuocdoituongkhuyetat")
            thanhvien = thanhvien.filter(thuocdoituongkhuyetat=request.POST.get("thuocdoituongkhuyetat"))
        if request.POST.get("tuoi"):
            search = search + '&tuoi=' + request.POST.get("tuoi")
            if request.POST.get("tuoi") == '1':
                thanhvien = thanhvien.filter(thuocdoituongbodoixuatngu=True)
            elif request.POST.get("tuoi") == '2':
                thanhvienObj = []
                for mem in thanhvien:
                    tuoi = calculate_age(mem.ngaysinh)
                    if tuoi == 17:
                        thanhvienObj.append(mem)
                thanhvien = thanhvienObj
            else:
                thanhvienObj = []
                for mem in thanhvien:
                    tuoi = calculate_age(mem.ngaysinh)
                    if tuoi <= 6:
                        thanhvienObj.append(mem)
                thanhvien = thanhvienObj
        start_age = form_thanhvien.cleaned_data['start_age']
        end_age = form_thanhvien.cleaned_data['end_age']
        if start_age and end_age:
            thanhvienObj = []
            for mem in thanhvien:
                tuoi = calculate_age(mem.ngaysinh)
                if tuoi >= int(start_age) and tuoi <= int(end_age):
                    thanhvienObj.append(mem)
            thanhvien = thanhvienObj
        
        context.update({'form_thanhvien': form_thanhvien, 'search': search})
        request.session['thanhvien'] = thanhvien
        request.session['form_thanhvien'] = form_thanhvien
    
    if 'thanhvien' in request.session:
        thanhvien = request.session['thanhvien']
        print thanhvien
        if len(thanhvien) > 0:
            if request.GET.get("orderby"):
                thanhvien = thanhvien.order_by('hogiadinh__todanpho')
                orderby = '&orderby=' + request.GET.get("orderby")
                context.update({'orderby': orderby})
            paginator = Paginator(thanhvien, 10)
            page = request.GET.get('page')
            try:
                thanhvien = paginator.page(page)
            except PageNotAnInteger:
                thanhvien = paginator.page(1)
            except EmptyPage:
                thanhvien = paginator.page(paginator.num_pages)
            context.update({'thanhvien': thanhvien})
        else:
            context.update({'message': 'Không có kết quả nào'})
    else:
        context.update({'message': 'Không có kết quả nào'})
    if 'form_thanhvien' in request.session:
        context.update({'form_thanhvien': request.session['form_thanhvien']})
    return render_to_response('timkiem/timkiemthanhvien.html', context, context_instance=RequestContext(request))

def xuattimkiemthanhvien(request):
    import xls_export
    if 'thanhvien' in request.session:
        thanhvien = request.session['thanhvien']
    else:
        thanhvien = []
    workbook = xls_export.export_timkiemthanhvien(thanhvien)

    response = HttpResponse(content_type='application/xls')
    response['Content-Disposition'] = 'attachment; filename="danhsachthanhvien.xls"'

    workbook.save(response)

    return response

@login_required
def nhapxls(request, file_type=None):
    hogiadinh_message = ''
    thanhvien_message = ''
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            if file_type == 'hogiadinh':
                try:
                    wb = xlrd.open_workbook(file_contents=request.FILES['hogiadinh_file'].read())
                    sheet = wb.sheet_by_index(0)
                    sheet = wb.sheet_by_name('Sheet1')
                    for row in range(1,sheet.nrows):
                        if not HoGiaDinh.objects.filter(masoho=sheet.cell(row,0).value):
                            ho = HoGiaDinh()
                            ho.masoho = sheet.cell(row,0).value
                            ho.sonha = sheet.cell(row,1).value
                            ho.todanpho = sheet.cell(row,2).value
                            ho.tinhtrangcutru = sheet.cell(row,3).value
                            ho.thuochongheo = sheet.cell(row,4).value
                            ho.thuochochinhsach = True if sheet.cell(row,5).value=='co' else False
                            ho.thuochocodoituongbaotro = True if sheet.cell(row,6).value=='co' else False
                            ho.cogiaychungnhanquyensdd = sheet.cell(row,7).value
                            ho.sodienthoai = sheet.cell(row,8).value
                            ho.tienthue = sheet.cell(row,9).value
                            ho.save()
                    hogiadinh_message = 'Bạn đã nhập thành công'
                except:
                    hogiadinh_message = 'File bạn đă nhập không đúng.'
            else:
                try:
                    wb = xlrd.open_workbook(file_contents=request.FILES['thanhvien_file'].read())
                    sheet = wb.sheet_by_index(0)
                    sheet = wb.sheet_by_name('Sheet1')
                    for row in range(1,sheet.nrows):
                        thanhvien = ThanhVien()
                        hogiadinh = HoGiaDinh.objects.get(masoho=sheet.cell(row,0).value)
                        thanhvien.hogiadinh = hogiadinh
                        thanhvien.hotenthanhvien = sheet.cell(row,1).value
                        thanhvien.ngaysinh = convert_datetime(sheet.cell(row,2).value)
                        thanhvien.gioitinh = sheet.cell(row,3).value
                        thanhvien.quequan = sheet.cell(row,4).value
                        thanhvien.quanhevoichuho = sheet.cell(row,5).value
                        thanhvien.thuocdoituongbodoixuatngu = True if sheet.cell(row,6).value=='co' else False
                        thanhvien.thuocdoituongbaotro = sheet.cell(row,7).value
                        thanhvien.thuocdoituongxichlo = True if sheet.cell(row,8).value=='co' else False
                        thanhvien.cmnd = sheet.cell(row,9).value
                        thanhvien.thuocdoituongkhuyetat = sheet.cell(row,10).value
                        thanhvien.thuocdoituongchinhsach = sheet.cell(row,11).value
                        thanhvien.cobaohiem = True if sheet.cell(row,12).value=='co' else False
                        thanhvien.tinhtranghonnhan = sheet.cell(row,13).value
                        thanhvien.trinhdo = sheet.cell(row,14).value
                        thanhvien.chuyenmon = sheet.cell(row,15).value
                        thanhvien.tongiao = sheet.cell(row,16).value
                        thanhvien.save()
                        if thanhvien.quanhevoichuho.lower() == 'chu ho':
                            hogiadinh.chuho = thanhvien.id
                            hogiadinh.save()
                    thanhvien_message = 'Bạn đã nhập thành công'
                except:
                    thanhvien_message = 'File bạn đă nhập không đúng.'
    else:
        form = UploadFileForm()

    context = {'form': form, 'hogiadinh_message': hogiadinh_message, 'thanhvien_message': thanhvien_message}
    return render_to_response('nhapcsv.html', context, context_instance=RequestContext(request))

#@login_required
@user_passes_test(lambda u: u.is_superuser)
def nguoidung(request, manguoidung=None):
    print request.user.is_superuser
    if manguoidung is None:
        form = NguoidungForm()
        nguoidung = None
    else:
        nguoidung = NguoiDung.objects.get(id=manguoidung)
        update_data = {'fullname':nguoidung.tennguoidung,
                       'username': nguoidung.user.username,
                       'permission': nguoidung.phanquyen.id,
                       'password' : nguoidung.user.password,
                       're_password' : nguoidung.user.password,
                       }
        form = NguoidungForm(initial=update_data)
    if request.method == 'POST':
        if 'user_id' in request.POST:
            nguoidung = NguoiDung.objects.get(id=int(request.POST['user_id']))
            user = nguoidung.user
            nguoidung.delete()
            user.delete()
            logging.info('-' * 60)
            logging.info(u'%s đã xóa người dùng %s' %(request.user.get_profile().tennguoidung, nguoidung.tennguoidung))
        else:
            form = NguoidungForm(request.POST)
            if form.is_valid():
                user = form.save(nguoidung)
                logging.info('-' * 60)
                logging.info(u'%s đã cập nhập người dùng %s' %(request.user.get_profile().tennguoidung, nguoidung))
    users = User.objects.all()
    context = {'users':users, 'form':form, 'quanlyPage': 'active'}
    return render_to_response('quanlynguoidung.html', context, context_instance=RequestContext(request))

@user_passes_test(lambda u: u.is_superuser)
def phanquyen(request, maquyen=None):
    phanquyen = PhanQuyen.objects.all()
    if maquyen is None:
        init_data = {'truongchinhsua': 
                     ['hotenthanhvien', 'ngaysinh', 'gioitinh', 'quequan', 'quanhevoichuho',
                      'cmnd', 'tinhtranghonnhan', 'trinhdo', 'chuyenmon', 'tongiao', 'dantoc']
                     }
        form = PhanQuyenForm(initial=init_data)
        quyen = None
    else:
        quyen = PhanQuyen.objects.get(id=maquyen)
        update_data = {'tenquyen':quyen.tenquyen, 'truongchinhsua': quyen.truongchinhsua.split(', ')}
        form = PhanQuyenForm(initial=update_data)
    if request.method == 'POST':
        if 'per_id' in request.POST:
            quyen = PhanQuyen.objects.get(id=int(request.POST['per_id']))
            quyen.delete()
            logging.info('-' * 60)
            logging.info(u'%s đã xóa phòng ban %s' %(request.user.get_profile().tennguoidung, quyen))
        else:
            form = PhanQuyenForm(request.POST)
            if form.is_valid():
                quyen = form.save(quyen)
                logging.info('-' * 60)
                logging.info(u'%s đã cập nhập phòng ban %s' %(request.user.get_profile().tennguoidung, quyen))
    context = {'phanquyen':phanquyen, 'form':form, 'per_fields': PERMISSION_FIELDS, 'quanlyPage': 'active'}
    return render_to_response('quanlyphanquyen.html', context, context_instance=RequestContext(request))

def danhsachchuho(request):
    thanhvien = ThanhVien.objects.all()
    chuho = []
    for mem in thanhvien:
        if mem.id == mem.hogiadinh.chuho:
            chuho.append(mem)
    
    paginator = Paginator(chuho, 100)
    page = request.GET.get('page')
    try:
        chuho = paginator.page(page)
    except PageNotAnInteger:
        chuho = paginator.page(1)
    except EmptyPage:
        chuho = paginator.page(paginator.num_pages)
    context = {'chuho':chuho, 'baocaoPage': 'active'}
    if not chuho:
        message = 'Chưa có thông tin nào'
        context.update({'message': message})
    return render_to_response('baocao/danhsachchuho.html', context, context_instance=RequestContext(request))

def xuatdanhsachchuho(request):
    import xls_export
    workbook = xls_export.export_chuho()

    response = HttpResponse(content_type='application/xls')
    response['Content-Disposition'] = 'attachment; filename="danhsachchuho.xls"'

    workbook.save(response)

    return response

def danhsachhogiadinh(request):
    hogiadinh = HoGiaDinh.objects.all()
    hogiadinhList = []
    for ho in hogiadinh:
        chuho = ThanhVien.objects.get(id=ho.chuho)
        hogiadinhObj = {
                        'masoho': ho.masoho,
                        'todanpho': ho.todanpho,
                        'tenchuho': chuho.hotenthanhvien,
                        'ngaysinh': chuho.ngaysinh,
                        'gioitinh': chuho.gioitinh,
                        'sonha': ho.sonha,
                        'sodienthoai': ho.sodienthoai,
                        'tinhtrangcutru': ho.tinhtrangcutru,
                        'thuochongheo': ho.thuochongheo,
                        'thuochochinhsach': ho.thuochochinhsach,
                        'thuocdoituongbaotro': ho.thuochocodoituongbaotro,
                        }
        hogiadinhList.append(hogiadinhObj)

    paginator = Paginator(hogiadinhList, 100)
    page = request.GET.get('page')
    try:
        hogiadinhList = paginator.page(page)
    except PageNotAnInteger:
        hogiadinhList = paginator.page(1)
    except EmptyPage:
        hogiadinhList = paginator.page(paginator.num_pages)
    context = {'hogiadinh':hogiadinhList, 'baocaoPage': 'active'}
    if not hogiadinhList:
        message = 'Chưa có thông tin nào'
        context.update({'message': message})
    return render_to_response('baocao/danhsachhogiadinh.html', context, context_instance=RequestContext(request))

def xuatdanhsachhogiadinh(request):
    import xls_export
    workbook = xls_export.export_danhsachhogiadinh()

    response = HttpResponse(content_type='application/xls')
    response['Content-Disposition'] = 'attachment; filename="danhsachhogiadinh.xls"'

    workbook.save(response)

    return response

def danhsachthuongbenhbinh(request):
    thanhvien = ThanhVien.objects.filter(Q(thuocdoituongchinhsach__contains='Thương binh') | Q(thuocdoituongchinhsach__contains='Bệnh binh'))
    
    paginator = Paginator(thanhvien, 100)
    page = request.GET.get('page')
    try:
        thanhvien = paginator.page(page)
    except PageNotAnInteger:
        thanhvien = paginator.page(1)
    except EmptyPage:
        thanhvien = paginator.page(paginator.num_pages)
    context = {'thanhvien':thanhvien, 'baocaoPage': 'active'}
    if not thanhvien:
        message = 'Chưa có thông tin nào'
        context.update({'message': message})
    return render_to_response('baocao/danhsachthuongbenhbinh.html', context, context_instance=RequestContext(request))

def xuatdanhsachthuongbenhbinh(request):
    import xls_export
    workbook = xls_export.export_danhsachthuongbenhbinh()

    response = HttpResponse(content_type='application/xls')
    response['Content-Disposition'] = 'attachment; filename="danhsachthuongbenhbinh.xls"'

    workbook.save(response)

    return response

def danhsachhongheo(request):
    hogiadinh = HoGiaDinh.objects.filter(~Q(thuochongheo = ''))
    hogiadinhList = []
    for ho in hogiadinh:
        chuho = ThanhVien.objects.get(id=ho.chuho)
        hogiadinhObj = {
                        'masoho': ho.masoho,
                        'todanpho': ho.todanpho,
                        'tenchuho': chuho.hotenthanhvien,
                        'ngaysinh': chuho.ngaysinh,
                        'gioitinh': chuho.gioitinh,
                        'sonha': ho.sonha,
                        'sodienthoai': ho.sodienthoai,
                        'tinhtrangcutru': ho.tinhtrangcutru,
                        'thuochongheo': ho.thuochongheo,
                        'thuochochinhsach': ho.thuochochinhsach,
                        'thuocdoituongbaotro': ho.thuochocodoituongbaotro,
                        }
        hogiadinhList.append(hogiadinhObj)

    paginator = Paginator(hogiadinhList, 100)
    page = request.GET.get('page')
    try:
        hogiadinhList = paginator.page(page)
    except PageNotAnInteger:
        hogiadinhList = paginator.page(1)
    except EmptyPage:
        hogiadinhList = paginator.page(paginator.num_pages)
    context = {'hogiadinh':hogiadinhList, 'baocaoPage': 'active'}
    if not hogiadinhList:
        message = 'Chưa có thông tin nào'
        context.update({'message': message})
    return render_to_response('baocao/danhsachhongheo.html', context, context_instance=RequestContext(request))

def xuatdanhsachhongheo(request):
    import xls_export
    workbook = xls_export.export_danhsachhongheo()

    response = HttpResponse(content_type='application/xls')
    response['Content-Disposition'] = 'attachment; filename="danhsachhongheo.xls"'

    workbook.save(response)

    return response


def danhsachtreemduoi6(request):
    thanhvien = ThanhVien.objects.all()
    
    thanhvienObj = []
    for mem in thanhvien:
        tuoi = calculate_age(mem.ngaysinh)
        if tuoi <= 6:
            thanhvienObj.append(mem)
    thanhvien = thanhvienObj

    paginator = Paginator(thanhvien, 100)
    page = request.GET.get('page')
    try:
        thanhvien = paginator.page(page)
    except PageNotAnInteger:
        thanhvien = paginator.page(1)
    except EmptyPage:
        thanhvien = paginator.page(paginator.num_pages)
    context = {'thanhvien':thanhvien, 'baocaoPage': 'active'}
    if not thanhvien:
        message = 'Chưa có thông tin nào'
        context.update({'message': message})
    return render_to_response('baocao/danhsachtreemduoi6.html', context, context_instance=RequestContext(request))

def xuatdanhsachtreemduoi6(request):
    import xls_export
    workbook = xls_export.export_danhsachtreemduoi6()

    response = HttpResponse(content_type='application/xls')
    response['Content-Disposition'] = 'attachment; filename="danhsachtreemduoi6tuoi.xls"'

    workbook.save(response)

    return response


def danhsachdoituongchinhsach(request):
    thanhvien = ThanhVien.objects.filter(~Q(thuocdoituongchinhsach = ''))

    paginator = Paginator(thanhvien, 100)
    page = request.GET.get('page')
    try:
        thanhvien = paginator.page(page)
    except PageNotAnInteger:
        thanhvien = paginator.page(1)
    except EmptyPage:
        thanhvien = paginator.page(paginator.num_pages)
    context = {'thanhvien':thanhvien, 'baocaoPage': 'active'}
    if not thanhvien:
        message = 'Chưa có thông tin nào'
        context.update({'message': message})
    return render_to_response('baocao/danhsachdoituongchinhsach.html', context, context_instance=RequestContext(request))

def xuatdanhsachdoituongchinhsach(request):
    import xls_export
    workbook = xls_export.export_danhsachdoituongchinhsach()

    response = HttpResponse(content_type='application/xls')
    response['Content-Disposition'] = 'attachment; filename="danhsachdoituongchinhsach.xls"'

    workbook.save(response)

    return response


def danhsachdoituongbaotro(request):
    thanhvien = ThanhVien.objects.filter(~Q(thuocdoituongbaotro = ''))

    paginator = Paginator(thanhvien, 100)
    page = request.GET.get('page')
    try:
        thanhvien = paginator.page(page)
    except PageNotAnInteger:
        thanhvien = paginator.page(1)
    except EmptyPage:
        thanhvien = paginator.page(paginator.num_pages)
    context = {'thanhvien':thanhvien, 'baocaoPage': 'active'}
    if not thanhvien:
        message = 'Chưa có thông tin nào'
        context.update({'message': message})
    return render_to_response('baocao/danhsachdoituongbaotro.html', context, context_instance=RequestContext(request))

def xuatdanhsachdoituongbaotro(request):
    import xls_export
    workbook = xls_export.export_danhsachdoituongbaotro()

    response = HttpResponse(content_type='application/xls')
    response['Content-Disposition'] = 'attachment; filename="danhsachdoituongbaotro.xls"'

    workbook.save(response)

    return response

def danhsachdoituongbodoixuatngu(request):
    thanhvien = ThanhVien.objects.filter(thuocdoituongbodoixuatngu=True)

    paginator = Paginator(thanhvien, 100)
    page = request.GET.get('page')
    try:
        thanhvien = paginator.page(page)
    except PageNotAnInteger:
        thanhvien = paginator.page(1)
    except EmptyPage:
        thanhvien = paginator.page(paginator.num_pages)
    context = {'thanhvien':thanhvien, 'baocaoPage': 'active'}
    if not thanhvien:
        message = 'Chưa có thông tin nào'
        context.update({'message': message})
    return render_to_response('baocao/danhsachdoituongbodoixuatngu.html', context, context_instance=RequestContext(request))

def xuatdanhsachdoituongbodoixuatngu(request):
    import xls_export
    workbook = xls_export.export_danhsachdoituongbodoixuatngu()

    response = HttpResponse(content_type='application/xls')
    response['Content-Disposition'] = 'attachment; filename="danhsachdoituongbodoixuatngu.xls"'

    workbook.save(response)

    return response

@login_required
def taikhoan(request):
    context = {'per_fields': PERMISSION_FIELDS, 'truongchinhsua': request.user.get_profile().phanquyen.truongchinhsua.split(', ')}
    return render_to_response('taikhoan.html', context, context_instance=RequestContext(request))