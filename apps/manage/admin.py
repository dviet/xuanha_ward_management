from models import PhanQuyen, NguoiDung, HoGiaDinh, ThanhVien
from django.contrib import admin

admin.site.register(PhanQuyen)
admin.site.register(NguoiDung)
admin.site.register(HoGiaDinh)
admin.site.register(ThanhVien)