from django import template

register = template.Library()
    
@register.filter
def previous_three_page(value):
    return value - 3 

@register.filter
def next_three_page(value):
    return value + 3 
            