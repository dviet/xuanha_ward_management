from datetime import date, datetime

def calculate_age(born):
    today = date.today()
    try: 
        birthday = born.replace(year=today.year)
    except ValueError: # raised when birth date is February 29 and the current year is not a leap year
        birthday = born.replace(year=today.year, day=born.day-1)
    if birthday > today:
        return today.year - born.year - 1
    else:
        return today.year - born.year

def convert_datetime(dt):
    return datetime.strptime(dt, '%d/%m/%Y')

def convert_bool(bool):
    if bool == 'True':
        return True
    elif bool == 'False':
        return False
    else:
        return None