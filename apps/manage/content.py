# -*- coding: utf-8 -*-

CS_CHOICES = (
        (u'Bà mẹ VNAH', u'Bà mẹ VNAH'),
        (u'Thờ cúng Bà mẹ VNAH', u'Thờ cúng Bà mẹ VNAH'),
        (u'CB Lão thành CM, tiền khởi nghĩa', u'CB Lão thành CM, tiền khởi nghĩa'),
        (u'Đối tượng hưởng tuấn liệt sỹ', u'Đối tượng hưởng tuấn liệt sỹ'),
        (u'Đối tượng Hưởng tuất từ trần NĐ31', u'Đối tượng Hưởng tuất từ trần NĐ31'),
        (u'Thương binh 1/4', u'Thương binh 1/4'),
        (u'Thương binh 2/4', u'Thương binh 2/4'),
        (u'Thương binh 3/4', u'Thương binh 3/4'),
        (u'Thương binh 4/4', u'Thương binh 4/4'),
        (u'Bệnh binh 1/3', u'Bệnh binh 1/3'),
        (u'Bệnh binh 2/3', u'Bệnh binh 2/3'),
        (u'Người hoạt động KC nhiễm CĐHH', u'Người hoạt động KC nhiễm CĐHH'),
        (u'Người có công CM', u'Người có công CM'),
        (u'Đối tượng Tù yêu nước', u'Đối tượng Tù yêu nước'),
        (u'Đối tượng có công 1 lần', u'Đối tượng có công 1 lần'),
        (u'Thân nhân chủ yếu thờ cúng 1 liệt sỹ', u'Thân nhân chủ yếu thờ cúng 1 liệt sỹ'),
        (u'Thân nhân chủ yếu thờ cúng 2 liệt sỹ', u'Thân nhân chủ yếu thờ cúng 2 liệt sỹ'),
        (u'Thân nhân chủ yếu thờ cúng 3 liệt sỹ', u'Thân nhân chủ yếu thờ cúng 3 liệt sỹ'),
        (u'Thân nhân thứ yếu thờ cúng 1 liệt sỹ', u'Thân nhân thứ yếu thờ cúng 1 liệt sỹ'),
        (u'Thân nhân thứ yếu thờ cúng 2 liệt sỹ', u'Thân nhân thứ yếu thờ cúng 2 liệt sỹ'),
        (u'Thân nhân thứ yếu thờ cúng 3 liệt sỹ', u'Thân nhân thứ yếu thờ cúng 3 liệt sỹ'),
        (u'Người hoạt động kháng chiến', u'Người hoạt động kháng chiến')
    )
CUTRU_CHOICES = (
        (u'Thường trú', u'Thường trú'),
        (u'Tạm trú', u'Tạm trú'),       
    )

HONGHEO_CHOICES = (
        (u'Hộ nghèo', u'Hộ nghèo'),
        (u'Hộ đặc biệt nghèo nhóm I', u'Hộ đặc biệt nghèo nhóm I'),
        (u'Hộ đặc biệt nghèo nhóm II', u'Hộ đặc biệt nghèo nhóm II'),
        (u'Hộ cận nghèo', u'Hộ cận nghèo'),
    )

BTXH_CHOICES = (
        (u'Trẻ em mồ côi - nhóm 1 NĐ67', u'Trẻ em mồ côi - nhóm 1 NĐ67'),
        (u'Người già cô đơn - Nhóm 2.1 NĐ67', u'Người già cô đơn - Nhóm 2.1 NĐ67'),
        (u'Người già cô đơn - Nhóm 2.2 NĐ67', u'Người già cô đơn - Nhóm 2.2 NĐ67'),
        (u'Người cao tuổi - Nhóm 3.1 NĐ67', u'Người già cô đơn - Nhóm 3.1 NĐ67'),
        (u'Người cao tuổi - Nhóm 3.2 NĐ67', u'Người già cô đơn - Nhóm 3.2 NĐ67'),
        (u'Người cao tuổi - Nhóm 3.3 NĐ67', u'Người già cô đơn - Nhóm 3.3 NĐ67'),
        (u'Người cao tuổi - Nhóm 3.4 NĐ67', u'Người già cô đơn - Nhóm 3.4 NĐ67'),
        (u'Người tàn tật - Nhóm 4.1 NĐ67', u'Người tàn tật - Nhóm 4.1 NĐ67'),
        (u'Người tàn tật - Nhóm 4.2 NĐ67', u'Người tàn tật - Nhóm 4.2 NĐ67'),
        (u'Người tâm thần - Nhóm 5 NĐ67', u'Người tâm thần - Nhóm 5 NĐ67'),
        (u'Người Nhiễm HIV không còn khả năng lao động - Nhóm 6 NĐ67', u'Người Nhiễm HIV không còn khả năng lao động - Nhóm 6 NĐ67'),
        (u'Gia đình nuôi trẻ mồ côi - Nhóm 7.1 NĐ67', u'Gia đình nuôi trẻ mồ côi - Nhóm 7.1 NĐ67'),
        (u'Gia đình nuôi trẻ mồ côi - Nhóm 7.2 NĐ67', u'Gia đình nuôi trẻ mồ côi - Nhóm 7.2 NĐ67'),
        (u'Gia đình nuôi trẻ mồ côi - Nhóm 7.3 NĐ67', u'Gia đình nuôi trẻ mồ côi - Nhóm 7.3 NĐ67'),
        (u'GĐ có 2 người tàn tật nặng trở lên - Nhóm 8.1 NĐ67', u'GĐ có 2 người tàn tật nặng trở lên - Nhóm 8.1 NĐ67'),
        (u'GĐ có 2 người tàn tật nặng trở lên - Nhóm 8.2 NĐ67', u'GĐ có 2 người tàn tật nặng trở lên - Nhóm 8.2 NĐ67'),
        (u'GĐ có 2 người tàn tật nặng trở lên - Nhóm 8.3 NĐ67', u'GĐ có 2 người tàn tật nặng trở lên - Nhóm 8.3 NĐ67'),
        (u'Người đơn thân hộ nghèo nuôi con dưới 16 tuổi - Nhóm 9.1 NĐ67', u'Người đơn thân hộ nghèo nuôi con dưới 16 tuổi - Nhóm 9.1 NĐ67'),
        (u'Người đơn thân hộ nghèo nuôi con dưới 16 tuổi - Nhóm 9.2 NĐ67', u'Người đơn thân hộ nghèo nuôi con dưới 16 tuổi - Nhóm 9.2 NĐ67'),
        (u'Người đơn thân hộ nghèo nuôi con dưới 16 tuổi - Nhóm 9.3 NĐ67', u'Người đơn thân hộ nghèo nuôi con dưới 16 tuổi - Nhóm 9.3 NĐ67'),
        (u'Đối tượng mất sức lao động', u'Đối tượng mất sức lao động'),
        (u'Đối tượng Bộ đội tâm thần', u'Đối tượng Bộ đội tâm thần'),
        (u'Người cao tuổi hưởng tuát từ trần', u'Người cao tuổi hưởng tuát từ trần'),
        (u'Đối tượng theo QĐ 62', u'Đối tượng theo QĐ 62'),
        (u'Đối tượng theo QĐ 874', u'Đối tượng theo QĐ 874'),
        (u'Đối tượng theo QĐ 14', u'Đối tượng theo QĐ 14'),
        (u'Đối tượng khác', u'Đối tượng khác')
)

KHUYETTAT_CHOICES = (
        (u'Khó khăn nghe', u'Khó khăn nghe'),
        (u'Khó khăn nhìn', u'Khó khăn nhìn'),
        (u'Khó khăn nói', u'Khó khăn nói'),
        (u'Khó khăn vận động', u'Khó khăn vận động'),
        (u'Đối tượng khác', u'Đối tượng khác')
)

HONNHAN_CHOICES = (
        (u'Chưa đăng ký kết hôn lần nào,', u'Chưa đăng ký kết hôn lần nào,'),
        (u'Đã kết hôn,', u'Đã kết hôn,'),
        (u'Đã kết hôn và ly hôn,', u'Đã kết hôn và ly hôn,'),
        (u'Đã kết hôn và vợ chết hoặc chồng chết)', u'Đã kết hôn và vợ chết hoặc chồng chết)'),
)

GENDER_CHOICES = (
    (u'Nam', u"Nam"),
    (u'Nữ', u"Nữ"),
)

PERMISSION_CHOICES = (
    ('hotenthanhvien', 'Họ tên thành viên'),
    ('ngaysinh', 'Ngày sinh'),
    ('gioitinh', 'Giới tính'),
    ('quequan', 'Quê quán'),
    ('quanhevoichuho', 'Quan hệ với chủ hộ'),
    ('thuocdoituongbodoixuatngu', 'Thuộc đối tượng bộ dội xuất ngũ'),
    ('thuocdoituongbaotro', 'Thuộc đối tượng bảo trợ'),
    ('thuocdoituongxichlo', 'Thuộc đối tượng xích lô'),
    ('cmnd', 'Số chứng minh nhân dân'),
    ('thuocdoituongkhuyetat', 'Thuộc đối tượng khuyết tật'),
    ('thuocdoituongchinhsach', 'Thuộc đối tượng chính sách'),
    ('cobaohiem', 'Có bảo hiểm'),
    ('tinhtranghonnhan', 'Tình trạng hôn nhân'),
    ('trinhdo', 'Trình độ'),
    ('chuyenmon', 'Chuyên môn'),
    ('tongiao', 'Tôn giáo'),
    ('dantoc', 'Dân tộc')
)

PERMISSION_FIELDS  = [
    ('hotenthanhvien', 'Họ tên thành viên'),
    ('ngaysinh', 'Ngày sinh'),
    ('gioitinh', 'Giới tính'),
    ('quequan', 'Quê quán'),
    ('quanhevoichuho', 'Quan hệ với chủ hộ'),
    ('thuocdoituongbodoixuatngu', 'Thuộc đối tượng bộ dội xuất ngũ'),
    ('thuocdoituongbaotro', 'Thuộc đối tượng bảo trợ'),
    ('thuocdoituongxichlo', 'Thuộc đối tượng xích lô'),
    ('cmnd', 'Số chứng minh nhân dân'),
    ('thuocdoituongkhuyetat', 'Thuộc đối tượng khuyết tật'),
    ('thuocdoituongchinhsach', 'Thuộc đối tượng chính sách'),
    ('cobaohiem', 'Có bảo hiểm'),
    ('tinhtranghonnhan', 'Tình trạng hôn nhân'),
    ('trinhdo', 'Trình độ'),
    ('chuyenmon', 'Chuyên môn'),
    ('tongiao', 'Tôn giáo'),
    ('dantoc', 'Dân tộc')
]

AGE_CHOICES = (
    (u'', u"Không chọn"),
    (u'1', u"Là bộ đội xuất ngũ"),
    (u'2', u"Là đối tượng đăng ký tuổi 17"),
    (u'3', u"Là trẻ em dưới 6 tuổi"),
)

YESNO_CHOICES = (
    (u'', u"Không chọn"),
    (u'1', u"Có"),
    (u'2', u"Không"),
)

CHECKBOX_CHOICES = (
    (None, u"Không chọn"),
    (True, u"Có"),
    (False, u"Không"),
)